/// Copyright 2021 Instreamatic
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// MARK: Install via Package Manager: https://github.com/Matovsky/SwiftTryCatch

import AVFoundation
import CoreTelephony
import EventKit
import MessageUI
import UIKit
import SwiftTryCatch

enum AdmanSectionType : Int {
    case sPreroll = 0
    case sMidroll
    case sPostroll
}

let TAG = "Adman"

let AdmanCurrentAd = adsList[activeAd] as? AdmanBannerWrapper
func AdmanGetAd(_ Index: Any) -> AdmanBanner? {
    adsList.object(at: Index) as? AdmanBanner
}

private let InstreamaticServer = "instreamatic.com"
// check network type
var networks_2g = [
    CTRadioAccessTechnologyGPRS,
    CTRadioAccessTechnologyEdge,
    CTRadioAccessTechnologyCDMA1x
]
var networks_4g = [CTRadioAccessTechnologyLTE]
var info = CTTelephonyNetworkInfo()
var carrierType: String? = nil
var carriers = info.serviceCurrentRadioAccessTechnology
var carrierName = carriers?.keys.first

var currentRoute = AVAudioSession.sharedInstance().currentRoute
var output = currentRoute.outputs.first


var type: String?
var slot: String?
var params: String? = nil


var interval = 18 + arc4random_uniform(32)
var expires = AD_EXPIRATION_TIME_DEFAULT


var size = AdmanVastParserDelegate.getDefaultImageSize()
var ad = AdmanCurrentAd

var size = AdmanVastParserDelegate.getDefaultImageSize()

var session = AVAudioSession.sharedInstance()
var actionType = player?.userData as? String
var ad = AdmanCurrentAd
var voiceAction = (AdmanCurrentAd)?.responses[actionType ?? ""]
var ad = AdmanCurrentAd
var isSilence = actionType == "silence"
var action: AdmanInteraction?


var valList: [AnyHashable : Any]? = nil

var link = valList?["link"] as? AdmanResponseValue
var phone = valList?["phone"] as? AdmanResponseValue
var custom_intent = valList?["custom_intent"] as? AdmanResponseValue
var valList = [AnyHashable : Any](minimumCapacity: response.values.count)
var val: AdmanResponseValue?

var audio = valList["audio"] as? AdmanResponseValue
var link = valList["link"] as? AdmanResponseValue
var phone = valList["phone"] as? AdmanResponseValue
var custom_intent = valList["custom_intent"] as? AdmanResponseValue

var banner: (AdmanBanner?) -> Void
var error: (Error?) -> Void

@objc protocol AdmanDelegate: NSObjectProtocol {
    // MARK: Method called when adman state changed
    // MARK: - Parameter sender: Adman instance
    func admanStateDidChange(_ sender: Adman)

    // MARK: Only for voice recognition.
    // MARK: - Parameter result: recognized word or phrase, empty string if not
    @objc optional func phraseRecognized(_ result: [AnyHashable : Any])
    @objc optional func customVoiceIntentHandler()

    // MARK: Triggered when image tapped by user
    // MARK: - Parameter urlToNavigate: url, opened in internal browser
    @objc optional func bannerTouched(_ urlToNavigate: String?)
    // MARK: Triggered when default ad view(with control playback buttons) closed
    @objc optional func viewClosed()
    @objc optional func errorReceived(_ error: Error?)
    // MARK: Triggered when dtmf label recived
    @objc optional func dtmfIn()
    @objc optional func dtmfOut()
    // MARK: Triggered only for external debug
    @objc optional func log(_ message: String)
}

enum AdmanState : Int {
        /// default adman state
        case initial = 0
        /// preloading and parse creative from vast/json source
        case fetchingInfo
        /// ad can be played
        case readyForPlayback
        /// If creative contain more than one ad, ad changed when playback completed
        case adChangedForPlayback
        case playing
        case paused
        case playbackCompleted
        case stopped
        /// only for voice ads
        case voiceResponsePlaying
        case voiceInteractionStarted
        case voiceInteractionEnded
        /// error occurred during loading/playback, 11
        case error
        case typeMismatch
        case preloading
        case speechRecognizerStarted
        case speechRecognizerStopped
        /// no ads for this siteId, region, audio format or other params
        case adNone
    }

enum AdmanBannerType : Int {
        case t320x480 = 0
        case t320x50
    }

var sharedManager: Adman? = nil
var firstQuarterReported = false
var secondQuarterReported = false
var thirdQuarterReported = false

func idfaFunc() {
    if let idfa = properties?.idfa {
        params = String(format: "site_id=%ld&player_id=%ld&type=%@&slot=%@&idfa=%@&microphone=%li", Int(properties?.siteId ?? 0), playerId, type ?? "", slot ?? "", idfa, Int(isMicrophoneEnabled))
    }
}

var can_show: String? = nil

func getFunc() {
    if let get = statistic?.getServer() {
        can_show = "https://\(get).instreamatic.com/live/can_show.gif?\(params ?? "")"
    }
}

class Adman: NSObject, UIGestureRecognizerDelegate, NSXMLParserDelegate, VACRecognizerDelegate, AdmanDtmfDelegate, AdmanPlayerDelegate, AdmanVastParserDelegate {
    /// @property state current Adman state

    private var _state: AdmanState!
    var state: AdmanState! {
        get {
            _state
        }
        set(newState) {
            if newState == _state {
                return
            }

            if newState != .error {
                fallbackState = newState
            }
            _state = newState

            delegate?.admanStateDidChange(self)
        }
    }
    var fallbackState: AdmanState!
    private(set) var error: Error?
    weak var delegate: AdmanDelegate?
    /// @property adsList<AdmanBanner*> creatives list for show/playback on device
    var adsList: [AdmanBanner]?
    /// index of active ad in adsList
    private(set) var activeAd = 0
    /// @property mute mute/unmute playback for current audio ad

    private var _mute = false
    var mute: Bool {
        get {
            _mute
        }
        set(mute) {
            _mute = mute
            if player != nil {
                if mute {
                    player?.volume = 0.0
                } else {
                    player?.volume = volume
                }
            }
        }
    }
    /// @property volume audio ad volume

    private var _volume: Float = 0.0
    var volume: Float {
        get {
            _volume
        }
        set(volume) {
            _volume = volume
            if mute && volume > 0.0 {
                mute = false
            }
            if player != nil {
                player?.volume = volume
            }
        }
    }
    /// @property elapsedTime time played in seconds from audio start
    private(set) var elapsedTime = 0
    /// @property elapsedTimeString time played in seconds as string from audio start
    private(set) var elapsedTimeString: String?
    private(set) var estimatedTime = 0
    private(set) var estimatedTimeString: String?
    var progress: Float = 0.0
    var userId: String?
    var playerId = 0
    private(set) var transcriptedPhrase: String?
    private var statistic: AdmanStats?
    private var properties: AdmanSettings?
    private var intentTapped = false
    private var vastLiveDurationTimer: Timer?
    /// Player which is used for audio ads playback
    private var player: AdmanPlayer?
    /// Timer to refresh playback time. 1 tick = 1 second
    private var playbackProgressTimer: Timer?
    /// Timer for opening the socket connection to voice recognition server and for enabling the microphone before the main track stops
    private var voiceResponseDelayTimer: Timer?
    /// Timer for voice recognition. After 5 seconds the ‘audio.end’ is to be sent to server
    private var interactionTimeoutTimer: Timer?
    /// Timer that used to send the ‘audio.end’ after the pre-defined time period
    private var endSpeechDetectionTimer: Timer?
    private var networkStatus: NetworkStatus?
    private var vastLoader: AdmanVastParser?
    private var dtmfListener: AdmanWebsocketDtmfListener?
    private var recognizer: VACRecognizer?
    private var cache: [AnyHashable : Any]?
    /// The amount of ‘unknown’ target actions. (Received from VAST)
    private var unknownPhraseIteration = 0
    private var allowPreload = false
    private var silentPreload = false

    /// Send request to server for creative.
    /// When response will be parsed and audio ad loaded, Adman state changed to ReadyForPlayback
    /// Adman state may be AdmanStateAdNone if server return empty response
    func prepare() {
    }

    func prepare(with format: AdmanFormat) {
    }

    func prepare(with type: AdmanType) {
    }

    func prepare(_ format: AdmanFormat, type: AdmanType) {
    }

    /// - Parameters:
    ///   - maxDuration: limit in seconds summary ads playback duration. Set 0 for default value, but cannot be lower than 10.
    ///   - adsCount: limit maximum ads count requested from server, 1 by default.
    func prepare(withAdLimits maxDuration: Int, adsCount: Int) {
    }

    func prepare(_ format: AdmanFormat, type: AdmanType, maxDuration: Int, adsCount: Int) {
    }

    func isValidToPlayback() -> Bool {
    }

    /// Cancel ad loading and audio buffering
    func cancelLoading() {
    }

    /// Start playback, Adman state changed to AdmanStatePlaying
    func play() {
    }

    /// Pause playback, state changed to AdmanStatePaused
    func pause() {
    }

    /// Resume playback, state changed to AdmanStatePlaying
    /// Playback cannot be resumed after [adman stop] method
    func resume() {
    }

    /// Stop playback and rewind to start.
    /// State changed to AdmanStatePlaying
    func rewind() {
    }

    /// Stop playback.
    /// State changed to AdmanStateStopped. Playback cannot be resumed.
    func stop() {
    }

    /// Place banner image in uiview
    /// - Parameters:
    ///   - spot: image view to place in
    ///   - type: image size
    func showBanner(forSpot spot: UIView, with type: AdmanBannerType) {
    }

    /// Report ad event
    /// - Parameter eventName: VAST-specific event name (see https://www.iab.com/guidelines/digital-video-ad-serving-template-vast-3-0 for more information about ad event names)
    /// first eventName letter should be always in lowerCase (e.g creativeView, clickTracking)
    func reportAdEvent(_ eventName: String) {
    }

    /// Return active Adman creative
    func getActiveAd() -> AdmanBanner? {
    }

    /// Override default banner size on preloading resources phase if creative does not contain 640x640 image
    /// - Parameter size: banner WxH, where W - width, H - size
    func setDefaultBannerSize(_ size: String) {
    }

    func getDefaultBannerSize() -> String {
    }

    /// - Parameters:
    ///   - format: any supported on Apple devices media format (e.g. mp3, aac, alac)
    ///   - bitrate: only float or integer (e.g. 128000, 128, 128.419)
    func setDefaultAudioFormat(_ format: String, withBitrate bitrate: NSNumber?) {
    }

    func setServerVoiceActivityDetection(_ state: Bool) {
    }

    class func setPreloadMode(_ mode: AdmanPreloadMode) {
    }

    /// Set custom user ip if needed (advanced targeting)
    func overrideIP(_ ip: String) {
    }

    /// Set default voice recognition backend
    func setRecognitionBackend(_ backend: AdmanRecognitionBackend) {
    }

    /// Return current region
    func currentRegion() -> AdmanRegion {
    }

    /// Return instance siteId
    func siteId() -> Int {
    }

    /// Return current libAdman version in format "X.Y.Z"
    class func getVersion() -> String {
    }

    /// Ad request. As for now, on play event `requestSection:success:failure` should be called for `sPreroll`. As a responce you will get the ad information - AdmanBanner object
    /// - Parameter section: requsted ad type: pre-roll, mid-roll, post-roll.
    /// Build and return full URL for Instreamatic voice recognition server

    /// Check if application is in background
    /// - Parameter siteId: unique partner identifier, obtained from the Instreamatic manager
    class func sharedManager(withSiteId siteId: Int) -> Self {

        {
            sharedManager = Adman(siteId: siteId)
        }

        return sharedManager!
    }

    /// - Parameter testMode: ignore all targeting params
    class func sharedManager(withSiteId siteId: Int, testMode: Bool) -> Self {

        {
            sharedManager = Adman(siteId: siteId, testMode: testMode)
        }

        return sharedManager!
    }

    class func sharedManager(withSiteId siteId: Int, playerId: Int, testMode: Bool) -> Self {

        { [self] in
            sharedManager = Adman(siteId: siteId, testMode: testMode, withZoneId: 0, region: AdmanRegionEU, playerId: playerId)
        }

        return sharedManager!
    }

    /// Initialize and return new Adman object
    class func sharedManager() -> Self {
        if sharedManager == nil {
            NSException.raise(
                NSExceptionName.invalidArgumentException,
                format: "Use sharedManagerWithSiteId:(NSInteger)siteId testMode:(BOOL) testMode first!")
        }
        return sharedManager!
    }

    class func sharedManager(withSiteId siteId: Int, withZoneId zoneId: Int, testMode: Bool) -> Self {

        {
            sharedManager = Adman(siteId: siteId, testMode: testMode, withZoneId: zoneId, region: AdmanRegionEU, playerId: 0)
        }

        return sharedManager!
    }

    class func sharedManager(withSiteId siteId: Int, region: AdmanRegion, playerId: Int, testMode: Bool) -> Self {


        { [self] in
            sharedManager = Adman(siteId: siteId, testMode: testMode, withZoneId: 0, region: region, playerId: playerId)
        }

        return sharedManager!
    }

    class func sharedManager(withSiteId siteId: Int, region: AdmanRegion, testMode: Bool) -> Self {

        {
            sharedManager = Adman(siteId: siteId, testMode: testMode, withZoneId: 0, region: region, playerId: 0)
        }

        return sharedManager!
    }

    convenience init(siteId: Int) {
        self.init(siteId: siteId, testMode: false, withZoneId: 0, region: AdmanRegionEU, playerId: 0)
    }

    convenience init(siteId: Int, testMode: Bool) {
        self.init(siteId: siteId, testMode: testMode, withZoneId: 0, region: AdmanRegionEU, playerId: 0)
    }

    convenience init(url: String?) {
        self.init(siteId: 0)
        properties?.requestURL = url
    }

    init(siteId: Int, testMode: Bool, withZoneId zoneId: Int, region: AdmanRegion, playerId: Int) {
        super.init()
        self.playerId = playerId
        properties = AdmanSettings()
        properties?.region = region == nil ? AdmanRegionEU : region
        properties?.siteId = siteId
        properties?.zoneId = zoneId

        recognizer = VACRecognizer()
        recognizer?.delegate = self

        if testMode {
            properties?.debugMode = NSNumber(value: Int32(AdmanDebugAudio))
        }

        statistic = AdmanStats()
        statistic?.region = properties?.region
        statistic?.save(
            for: AdmanStatEventLoad,
            point: "load",
            admanId: 3171,
            siteId: properties?.siteId,
            playerId: 0,
            campaignId: 0,
            bannerId: 0,
            deviceId: properties?.idfa)
        statistic?.save(
            for: AdmanStatEventRequest,
            point: "req",
            admanId: 3171,
            siteId: properties?.siteId,
            playerId: 0,
            campaignId: 0,
            bannerId: 0,
            deviceId: properties?.idfa)
        statistic?.save(
            for: AdmanStatEventFetched,
            point: "fetched",
            admanId: 3171,
            siteId: properties?.siteId,
            playerId: 0,
            campaignId: 0,
            bannerId: 0,
            deviceId: properties?.idfa)

        if properties?.region != AdmanRegionVoice {
            statistic?.reportEvent(AdmanStatEventLoad)
        }

        NotificationCenter.default.addObserver(
            self,
            selector: Selector("triggerActionAdmanControl:"),
            name: NOTIFICATION_ADMAN_CONTROL,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: Selector("triggerActionAdmanPlayer:"),
            name: NOTIFICATION_ADMAN_PLAYER,
            object: nil)

        player = AdmanPlayer()
        player?.delegate = self
        allowPreload = true
        self.state = .initial

        weak var weakSelf = self
        networkStatus = NetworkStatus.reachabilityForInternetConnection()
        networkStatus?.reachableBlock = { status in
            weakSelf?.networkStateDidChanged(status?.currentReachability() != NotReachable)
        }
        networkStatus?.unreachableBlock = { status in
            weakSelf?.networkStateDidChanged(status?.currentReachability() != NotReachable)
        }
        networkStatus?.startNotifier()
    }

    func setCustomRegionServer(_ adServer: String?) {
        properties?.region = AdmanRegionCustom
        properties?.customAdServer = adServer
    }

    func errorRecived(_ err: Error?, param: Int) {
        if delegate?.responds(to: NSSelectorFromString("errorReceived:")) ?? false && err != nil {
            delegate?.errorReceived?(err)
        }

        error = err
        print("Error recived: \(err?.localizedDescription ?? "")")
        resetRecognizerState()

        player?.pause()
        player?.replaceCurrentItem(withPlayerItem: nil)

        self.state = .error
        logBadRequest()
        NotificationAdmanBase.sendEvent(AdmanMessageEventError)
        if param == 0 {
            skip()
        } else if param == 1 {
            processIntent([
                "transcript": "No",
                "action": "negative",
                "playMicOff": "No"
            ])
        }
    }

    func errorRecived(_ err: Error?) {
        errorRecived(err, param: 0)
    }

    func openUrlSafe(_ url: String?, type linkType: Int) {
        
        SwiftTryCatch.try({
            print("\(TAG): Url should be \(url ?? "") opened")
            do {
                if (state as? NSNumber).intValue == 1 && linkType != Int(AdmanLinkTypeMobile) {
                    NotificationAdmanControl.sendEvent(AdmanControlEventLink, userData: URL(string: url ?? ""))
                } else {
                    if #available(iOS 10.0, *) {
                        if let url1 = URL(string: url ?? "") {
                            UIApplication.shared.open(url1, options: [:], completionHandler: nil)
                        }
                    }
                }
            }
        }, catch: { exception in
            print("Failed to open url: \(url ?? ""), \(exception.description())")
        }, finallyBlock: {
        })
    }

    /// Return Adman ad server for user Region
    func getServer(_ region: AdmanRegion) -> String? {
        switch region {
            case AdmanRegionGlobal:
                return "x3.\(InstreamaticServer)"
            case AdmanRegionVoice:
                return "voice.\(InstreamaticServer)"
            case AdmanRegionDemo:
                return "d1.\(InstreamaticServer)"
            case AdmanRegionDemoEu:
                return "test.\(InstreamaticServer)"
            case AdmanRegionIndia:
                return "x1-india.\(InstreamaticServer)"
            case AdmanRegionUS:
                return "d2975e0yphvxch.cloudfront.net"
            case AdmanRegionCustom:
                return properties?.customAdServer
            default:
                return "x.\(InstreamaticServer)"
        }
    }

    class func getRecognitionBackend(_ backend: AdmanRecognitionBackend) -> String? {
        switch backend {
            case AdmanRecognitionBackendNuanceDev:
                return "/nuance"
            case AdmanRecognitionBackendNuanceRelease:
                return "/nuance/prod"
            case AdmanRecognitionBackendHoundify:
                return "/houndify"
            case AdmanRecognitionBackendYandex:
                return "/yandex"
            case AdmanRecognitionBackendMicrosoft:
                return "/microsoft"
            case AdmanRecognitionBackendAuto:
                fallthrough
            default:
                return ""
        }
    }

    class func getVoiceServer(_ region: AdmanRegion) -> String? {
        switch region {
        // USA
            case AdmanRegionGlobal, AdmanRegionVoice:
                return "wss://v3.instreamatic.com"
        // Test voice server
            case AdmanRegionDemo:
                return "wss://voice.instreamatic.com"
        // EU
            default:
                return "wss://v2.instreamatic.com"
        }
    }

    class func getVoiceServer(_ region: AdmanRegion, location: AdmanRecognitionBackendLocation) -> String? {
        switch location {
            case AdmanRecognitionBackendLocationEU:
                return Adman.getVoiceServer(AdmanRegionEU)
            case AdmanRecognitionBackendLocationUSA:
                return Adman.getVoiceServer(AdmanRegionGlobal)
            case AdmanRecognitionBackendLocationDemo:
                return Adman.getVoiceServer(AdmanRegionDemo)
            default:
                return Adman.getVoiceServer(region)
        }
    }

    func getWebsocketHeaderOptions() -> [AnyHashable : Any]? {
        if let idfa = properties?.idfa, let adId = (AdmanCurrentAd)?.adId {
            return [
                "idfa": idfa,
                "site_id": NSNumber(value: properties?.siteId ?? 0),
                "vad": NSNumber(value: properties?.serverVoiceActivityDetection ?? false),
                "ad_id": adId,
                "response_delay": String(format: "%.4g", getResponseDelay()),
                "device_info": String(format: "Adman voice sdk %@; %@ %ld", AdmanVersion, UIDevice.current.model, ProcessInfo.processInfo.operatingSystemVersion.majorVersion)
            ]
        }
        return nil
    }

    func getSpeechRecognizerUrl(_ voiceServer: String?) -> String? {
        if (voiceServer as NSString?)?.range(of: "adman/").location != NSNotFound {
            return voiceServer
        }

        var ad: AdmanBannerWrapper? = nil
        var url: String? = nil
        if let backend = properties?.backend {
            url = "\(voiceServer ?? "")/adman\(Adman.getRecognitionBackend(backend) ?? "")/v2"
        }
        if ad?.responseURL {
            url = ad?.responseURL
        }

        if (adsList?.count ?? 0) > 0 {
            var crossServerAdID = ""
            ad = AdmanCurrentAd
            if ad?.crossServerAdID != nil {
                if let crossServerAdID1 = ad?.crossServerAdID {
                    crossServerAdID = "&ad_id=\(crossServerAdID1)"
                }
            }
            if let responseLang = ad?.responseLang {
                url = "\(url ?? "")?language=\(responseLang)\(crossServerAdID)"
            }
        }
        return url
    }

    /// Enable dtmf ads detection for station name
    /// - Parameters:
    ///   - name: radiostation name
    ///   - preload: ad aotimaticaly after short delay
    ///   - vc: ViewController for default sdk banner view to display in
    func enableDtmfAdsDetection(forStation name: String, preload: Bool, vc: UIViewController?) {
        // [_uiManager setBannerParentVC:vc];

        properties?.dtmfAdsDetection = true
        properties?.dtmfStationKey = name

        dtmfListener = AdmanWebsocketDtmfListener()
        dtmfListener?.delegate = self
        dtmfListener?.start(properties?.dtmfStationKey)

        if preload {
            prepareWithDelay()
        }
    }

    func disableDtmfAdsDetection() {
        properties?.dtmfAdsDetection = false
        dtmfListener?.stop()
        dtmfListener = nil
    }

    /// Check if app already in background
    func `is`(inBackground synced: Bool, then cb: AdmanOnFulfill) {
        if synced {
            if Thread.isMainThread {
                let state = UIApplication.shared.applicationState == .background
                cb(NSNumber(value: state))
            } else {
                DispatchQueue.main.sync(execute: { [self] in
                    let state = UIApplication.shared.applicationState == .background
                    cb(NSNumber(value: state))
                })
            }
        } else {
            DispatchQueue.main.async(execute: { [self] in
                let state = UIApplication.shared.applicationState == .background
                DispatchQueue.global(qos: .default).async(execute: { [self] in
                    cb(NSNumber(value: state))
                })
            })
        }
    }

    func hasNextAd() -> Bool {
        if adsList == nil {
            return false
        }
        return ((adsList?.count ?? 0) - 1) > activeAd
    }

    func playNextAd() {
        activeAd += 1
        unknownPhraseIteration = 0
        properties?.voiceResponseDelay = (AdmanCurrentAd)?.responseDelay
        self.state = .adChangedForPlayback
        NotificationAdmanPlayer.sendEvent(AdmanPlayerEventReady)
        play()

        recognizer?.stop()
        recognizer?.closeConnection()

        if let adId = (AdmanCurrentAd)?.adId {
            log("libAdman: start playing next ad(\(adId)) from sequence")
        }
    }

    func preloadIntro() {
        // TODO
    }

    func preloadAd(_ index: Int, onComplete fulfill: AdmanOnFulfill, onFail reject: AdmanOnFail) {
        log(
            String(format: "Fetching %li ad from queue. Banner: %@", index, (AdmanGetAd(index)).url))
        if (AdmanGetAd(index)).url != nil {
            let imageURL = URL(string: (AdmanGetAd(index)).url)
            if let imageURL = imageURL {
                (AdmanGetAd(index)).adCache = Data(contentsOf: imageURL)
            }
            let img = UIImage(data: (AdmanGetAd(index)).adCache)
            // preload image for fullscreen ad
            preload(img)
            (AdmanGetAd(index)).bannerImage = img
            log("Banner preloaded succefully")
        }

        let ad = (AdmanGetAd(index)) as? AdmanBannerWrapper
        if ad?.assets[AdmanBannerAssetMicOnUrl] != nil {
            var micOnLoaded = false
            var micOffLoaded = false
            AdmanPlayer.preloadAsset(ad?.assets[AdmanBannerAssetMicOnUrl], onComplete: { asset in
                micOnLoaded = true
                ad?.assets[AdmanBannerAssetMicOnCache] = asset
                if micOffLoaded && micOnLoaded {
                    fulfill(ad)
                }
            }, onFail: { err in
                micOnLoaded = true
                if micOffLoaded && micOnLoaded {
                    fulfill((AdmanGetAd(index)))
                }
            })
            AdmanPlayer.preloadAsset(ad?.assets[AdmanBannerAssetMicOffUrl], onComplete: { asset in
                micOffLoaded = true
                ad?.assets[AdmanBannerAssetMicOffCache] = asset
                if micOffLoaded && micOnLoaded {
                    fulfill(ad)
                }
            }, onFail: { err in
                micOffLoaded = true
                if micOffLoaded && micOnLoaded {
                    fulfill((AdmanGetAd(index)))
                }
            })
        } else {
            fulfill(ad)
        }
    }

    /// Check mirophone availabillity
    func isMicrophoneEnabled() -> Bool {
        switch AVAudioSession.sharedInstance().recordPermission {
            case .denied, .undetermined:
                return false
            default:
                return true
        }
    }

    func buildRequest(_ params: AdmanSettings?, cache: Bool) -> [AnyHashable : Any]? {
        var path: String?
        var supportedVASTVersion = VAST_VERSION_SUPPORT
        if properties?.region == AdmanRegionUS {
            supportedVASTVersion = 2
        }

        if properties?.region != AdmanRegionVoice && properties?.region != AdmanRegionDemo && properties?.region != AdmanRegionDemoEu {
            if let region = properties?.region {
                path = String(format: "https://%@/v%li/vast", getServer(region) ?? "", supportedVASTVersion)
            }
        } else {
            if let region = properties?.region {
                path = String(format: "https://%@/x/v%li/vast", getServer(region) ?? "", supportedVASTVersion)
            }
        }

        if properties?.siteId {
            path = (path ?? "") + String(format: "/%ld", Int(properties?.siteId ?? 0))
        }
        if playerId != 0 {
            path = (path ?? "") + String(format: "/%ld", playerId)
        }
        path = (path ?? "") + ".xml"
        if cache {
            self.cache?["requestURL"] = path
        }

        var requestParams: [AnyHashable : Any] = [:]
        requestParams["idfa"] = properties?.idfa
        requestParams["advertising_tracking_enabled"] = (properties?.isTrackingEnabled ? "1" : "0")
        requestParams["version"] = AdmanVersion

        var slot: String?
        switch properties?.slot {
            case AdmanPreroll:
                slot = "preroll"
            case AdmanMidroll:
                slot = "midroll"
            case AdmanPostroll:
                slot = "postroll"
            default:
                break
        }
        if let slot = slot {
            requestParams["slot"] = slot
        }

        if params?.type == AdmanTypeVoice || params?.type == AdmanTypeAny || params?.type == AdmanTypeVoiceMultipleAds {
            requestParams["microphone"] = NSNumber(value: Int(isMicrophoneEnabled())).stringValue
            if EKEventStore.authorizationStatus(for: .event) == .authorized {
                requestParams["calendar"] = "1"
            }
        }

        if properties?.debugMode && properties?.debugMode.intValue ?? 0 != 0 {
            requestParams["preview"] = properties?.debugMode
        }
        if properties?.clientIP {
            requestParams["ip"] = properties?.clientIP
        }

        if properties?.campaignId {
            requestParams["campaign_id"] = String(format: "%ld", Int(properties?.campaignId ?? 0))
        }
        if properties?.creativeId {
            requestParams["creative_id"] = String(format: "%ld", Int(properties?.creativeId ?? 0))
        }
        if properties?.zoneId {
            requestParams["zone_id"] = String(format: "%ld", Int(properties?.zoneId ?? 0))
        }
        if userId != nil {
            requestParams["user_id"] = userId
        }
        if properties?.format {
            requestParams["format"] = String(format: "%ld", Int(properties?.format ?? 0))
        }
        if properties?.maxAdsCount {
            requestParams["ads_count"] = String(format: "%ld", Int(properties?.maxAdsCount ?? 0))
        }
        if properties?.maxAdBlockDuration {
            requestParams["duration"] = String(format: "%ld", Int(properties?.maxAdBlockDuration ?? 0))
        }

        if properties?.useGeolocationTargeting {
            requestParams["lat"] = NSNumber(value: properties?.latitude ?? 0.0).stringValue
            requestParams["long"] = NSNumber(value: properties?.longitude ?? 0.0).stringValue
        }

        if let siteVariables = properties?.siteVariables {
            for key in siteVariables {
                guard let key = key as? String else {
                    continue
                }
                requestParams[key] = properties?.siteVariables.value(forKey: key)
            }
        }

        requestParams["lang"] = "\(NSLocale.preferredLanguages.first ?? "")"

        if properties?.type {
            Any?; state(bool)
            appIsInBG = (state as? NSNumber).intValue == 1

            switch properties?.type {
                case AdmanTypeAny:
                    if #available(iOS 12.0, *) {
                        if appIsInBG {
                            requestParams["type"] = "digital"
                        } else {
                            requestParams["type"] = "any"
                        }
                    } else {
                        requestParams["type"] = "any"
                    }
                case AdmanTypeAudioOnly:
                    requestParams["type"] = "radio"
                case AdmanTypeAudioPlus:
                    requestParams["type"] = "digital"
                case AdmanTypeVoice:
                    if #available(iOS 12.0, *) {
                        if appIsInBG {
                            requestParams["type"] = "digital"
                        } else {
                            requestParams["type"] = "voice"
                        }
                    } else {
                        requestParams["type"] = "voice"
                    }
                default:
                    break
            }
        }
    }
}

extension Adman {
    /// ##### Common setup methods #####
    /// Disable sdk AVAudioSession setup if app need to keep settings
    /// Enable/disable debug mode
    /// Enable preroll/midroll/postroll targeting
    /// Set voice response delay
    /// Set max lenght for server waiting timeout
    /// Set custom siteVariable to all ad requests
}

// preloading adfunc dispatch_get_main_queue() {


// set no limit for max ads duration if parameter lower than 10
/// Only for voice ads in background mode in WiFi networks
/// Sience iOS 12 release we cant turn microphone on when app in background
/// If application was in background and preloaded creative require voice activity
/// We need to change default ad type to audio only, preload it and play
//[_uiManager showPlayButton];
//[_uiManager showPauseButton];
//[_uiManager showPlayButton];
