/// Copyright 2021 Instreamatic
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// MARK: Install via Package Manager: https://github.com/Matovsky/SwiftTryCatch

import AdSupport
import SwiftTryCatch

enum AdmanDebugMode : Int {
    /// test creative with image and audio
    case admanDebugBanner = 11
    /// test creative with audio
    case admanDebugAudio = 12
    /// test voice creative
    case admanDebugVoiceAd = 41
    /// debug mode disabled
    case admanDebugNone = 0
}

enum AdmanType : Int {
    case any = 1
    case audioOnly = 7
    case audioPlus = 6
    case voice = 12
    case voiceMultipleAds
}

enum AdmanRegion : Int {
    case eu = 0
    case us
    case global
    case demo
    case demoEu
    case voice
    case india
    case custom
}

enum AdmanFormat : Int {
    case any = 1
    case swiftSponsor = 2
    case gameVibe = 3
}

enum AdmanAdSlot : Int {
    case admanPreroll
    case admanMidroll
    case admanPostroll
    case `default`
}

enum AdmanRecognitionBackend : Int {
    case auto = 0
    case yandex
    case microsoft
    case nuanceDev
    case houndify
    case nuanceRelease
}

let AdmanStatEventLoad = "load"
let AdmanStatEventRequest = "request"
let AdmanStatEventFetched = "fetched"

let AdmanStatEventClickTracking = "clickTracking"
let AdmanStatEventCreativeView = "creativeView"
let AdmanStatEventImpression = "impression"
let AdmanStatEventCanShow = "can_show"
let AdmanStatEventStart = "start"
let AdmanStatEventFirstQuartile = "firstQuartile"
let AdmanStatEventMidpoint = "midpoint"
let AdmanStatEventThirdQuartile = "thirdQuartile"
let AdmanStatEventComplete = "complete"
let AdmanStatEventPause = "pause"
let AdmanStatEventRewind = "rewind"
let AdmanStatEventResume = "resume"
let AdmanStatEventMute = "mute"
let AdmanStatEventUnmute = "unmute"
let AdmanStatEventClose = "close"
let AdmanStatEventError = "error"

let AdmanStatEventSkip = "skip"
let AdmanStatEventResponseNegative = "respose_negative"
let AdmanStatEventResponseSilence = "respose_silence"
let AdmanStatEventResponseUnknown = "respose_unknown"
let AdmanStatEventResponseOpen = "respose_open"
let AdmanStatEventResponseSkip = "respose_skip"
let AdmanStatEventResponseSwearing = "respose_swearing"
let AdmanStatEventResponseError = "respose_error"
let AdmanStatEventResponseOpenBanner = "response_open_banner"
let AdmanStatEventResponseOpenButton = "response_open_button"
let AdmanStatEventResponseNegativeButton = "response_negative_button"

/// geo location for voice recognition server
/// Auto - default value, replaced by VAST responseURL section if present.
/// EU
/// USA
/// Demo - USA demo server
enum AdmanRecognitionBackendLocation : Int {
    case auto
    case eu
    case usa
    case demo
}

private let INSTREAMATIC_STAT_URL = "https://%@.instreamatic.com/%@stat/%@.gif?v=%@"
private let INSTREAMATIC_LOGGING_URL = "https://%@/admin/api/app-log"
private let INSTREAMATIC_MOBILE_IOS_VALUE = "1"

class AdmanCreative: NSObject {
    /// @property statistics<NSString*, NSArray*<NSString*>> stat data from Creative tracking
    var tracking: [String : [String]] = [:]
    var staticResource = ""
    /// @property urlToNavigateOnClick link to open in internal browser
    var urlToNavigateOnClick: String?
}

class AdmanStatRequest: NSObject {
    var isReported = false
    var onComplete: (() -> Void)?
    var onFail: ((Error?) -> Void)?
    private var links: [String] = []

    override init() {
        super.init()
        if let array = [AnyHashable](repeating: 0, count: 2) as? [String] {
            links = array
        }
    }

    init(fromArray URIs: [AnyHashable]?) {
        super.init()
        if let URIs = URIs as? [String] {
            links = URIs
        }
    }

    func addURL(_ URL: String?) {
        links.append(URL ?? "")
    }

    func report() {
        if isReported {
            return
        }

        for link in links {
            request(link)
        }
    }

    func request(_ url: String?) {
        if url == nil {
            return
        }
        if isReported {
            return
        }

        SwiftTryCatch.try({
            isReported = true
            let config = URLSessionConfiguration.default
            config.timeoutIntervalForRequest = 7.0
            var task: URLSessionTask? = nil
            if let url1 = URL(string: url ?? "") {
                task = URLSession(configuration: config).dataTask(
                    with: url1,
                    completionHandler: { [self] data, response, err in
                        if err == nil {
                            if let onComplete = onComplete {
                                onComplete()
                            }
                        } else {
                            if let onFail = onFail {
                                onFail(err)
                            }
                        }
                    })
            }
            task?.resume()
        }, catch: { exception in
            print("Failed to report event for pixel: \(url ?? ""), recieved system error")
        }, finallyBlock: {
        })
    }
}

class AdmanStats: NSObject {
    var badRequests: [Date] = []
    /// URLs for statistics
    private var items: [String : AdmanStatRequest]?

    private var _region: AdmanRegion!
    private var region: AdmanRegion! {
        get {
            _region
        }
        set(region) {
            _region = region
        }
    }

    func setRegion(_ region: AdmanRegion) {
    }

    override init() {
        super.init()
        items = [:]
        if let array = [AnyHashable](repeating: 0, count: 4) as? [Date] {
            badRequests = array
        }

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(admanPlayerNotification(_:)),
            name: NOTIFICATION_ADMAN_PLAYER,
            object: nil)
    }

    override func value(forKey key: String) -> Any? {
        return items?[key]
    }

    func save(for event: String?, point: String?, admanId: Int, siteId: Int, playerId: Int, campaignId: String?, bannerId: Int, deviceId: String?) {

        var q: [AnyHashable : Any] = [:]
        q["m"] = INSTREAMATIC_MOBILE_IOS_VALUE
        if admanId != 0 {
            q["a"] = NSNumber(value: admanId).stringValue
        }
        if siteId != 0 {
            q["s"] = NSNumber(value: siteId).stringValue
        }
        if playerId != 0 {
            q["p"] = NSNumber(value: playerId).stringValue
        }
        if let campaignId = campaignId {
            q["c"] = campaignId
        }
        if bannerId != 0 {
            q["b"] = NSNumber(value: bannerId).stringValue
        }
        if let deviceId = deviceId {
            q["i"] = deviceId
        }

        var err: Error?
        var jsonData: Data? = nil
        do {
            jsonData = try JSONSerialization.data(withJSONObject: q, options: [])
        } catch let err {
        }
        var queryString: String? = nil
        if let jsonData = jsonData {
            queryString = String(data: jsonData, encoding: .utf8)
        }

        let data = queryString?.data(using: .utf8)
        let base64encodedString = data?.base64EncodedString(options: []).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)

        let url = String(format: INSTREAMATIC_STAT_URL, getServer() ?? "", (region == .demo) ? "xs/" : "", point ?? "", base64encodedString ?? "")
        let components = NSURLComponents(string: url)

        save(components?.url?.absoluteString, forEvent: event)
    }

    func save(_ url: String?, forEvent event: String?) {
        if items?[event] == nil {
            items?[event ?? ""] = AdmanStatRequest()
        }
        items?[event]?.addURL(url)
    }

    func reportEvent(_ eventName: String?) {
        let request = items?[eventName] as? AdmanStatRequest
        print("Reporting event \(eventName ?? "")")
        request?.report()
        request?.onComplete = {
            print("libAdman.stats: event \(eventName ?? "") reported succefully")
        }
        request?.onFail = { err in
            print("libAdman.stats: failed to report event \(eventName ?? ""), message: \(err?.localizedDescription ?? "")")
        }
    }

    func reportEvent(_ event: String?, forCompanion companion: AdmanCreative?) {
        if companion?.tracking != nil && companion?.tracking[event ?? ""] != nil {
            let request = AdmanStatRequest(fromArray: companion?.tracking[event ?? ""])
            request.report()
        }
    }

    func getServer() -> String? {
        switch region {
            case .global:
                return "xs3"
            case .voice:
                return "voice"
            case .demo:
                return "d1"
            default:
                return "xs"
        }
    }

    // MARK: Notifications

    @objc func admanPlayerNotification(_ notification: Notification?) {
        let dict = notification?.userInfo
        let message = dict?[NOTIFICATION_ADMAN_PLAYER_KEY] as? NotificationAdmanPlayer
        if message == nil {
            return
        }

        switch message?.event {
            case AdmanPlayerEventComplete:
                for key in items ?? [:] {
                    guard let key = key as? String else {
                        continue
                    }
                    items?[key]?.isReported = false
                }
            default:
                break
        }
    }
}
