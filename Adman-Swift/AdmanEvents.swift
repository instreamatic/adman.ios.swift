/// Copyright 2019 Instreamatic
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation
import UIKit

//if !AdmanEvents_h {
//#define AdmanEvents_h



let NOTIFICATION_ADMAN_MESSAGE = "NotificationMessageEventAdman"
let NOTIFICATION_ADMAN_MESSAGE_KEY = "adman_message"
enum AdmanMessageEventType : Int {
    case admanMessageEventFetchingInfo = 1
    case admanMessageEventPreloading
    case admanMessageEventNone
    case admanMessageEventError
    case admanMessageEventReady
    case admanMessageEventStarted
    case admanMessageEventStopped
    case admanMessageEventAlmostCompleted
    case admanMessageEventCompleted
    case admanMessageEventSkipped
}

let NOTIFICATION_ADMAN_PLAYER = "NotificationMessageEventAdmanPlayer"
let NOTIFICATION_ADMAN_PLAYER_KEY = "adman_player"
enum AdmanPlayerEventType : Int {
    case admanPlayerEventPrepare = 1
    case admanPlayerEventReady
    case admanPlayerEventFailed
    case admanPlayerEventPlaying
    case admanPlayerEventPlay
    case admanPlayerEventPause
    case admanPlayerEventProgress
    case admanPlayerEventComplete
    case admanPlayerEventCloseable
    case admanPlayerEventSkippable
}

let NOTIFICATION_ADMAN_VOICE = "NotificationMessageEventAdmanVoice"
let NOTIFICATION_ADMAN_VOICE_KEY = "adman_voice"
enum AdmanVoiceEventType : Int {
    case admanVoiceEventResponsePlaying = 1
    case admanVoiceEventInteractionStarted
    case admanVoiceEventInteractionEnded
    case admanVoiceEventError
    case admanVoiceEventRecognizerStarted
    case admanVoiceEventRecognizerStopped
}

let NOTIFICATION_ADMAN_CONTROL = "NotificationMessageEventAdmanControl"
let NOTIFICATION_ADMAN_CONTROL_KEY = "adman_control"
enum AdmanControlEventType : Int {
    case admanControlEventPrepare = 1
    case admanControlEventStart
    case admanControlEventPause
    case admanControlEventResume
    case admanControlEventSkip
    case admanControlEventClick
    case admanControlEventLink
    case admanControlEventSwipeBack
    case admanControlEventPositive
    case admanControlEventNegative
}

class NotificationAdmanBase: NSObject {
    var event: AdmanMessageEventType!

    init(_ event: AdmanMessageEventType) {
        super.init()
        self.event = event
    }

    class func sendEvent(_ event: AdmanMessageEventType) {
        let message = NotificationAdmanBase(event)
        let dict = [NOTIFICATION_ADMAN_MESSAGE_KEY : message]
        NotificationCenter.default.post(name: NSNotification.Name(NOTIFICATION_ADMAN_MESSAGE), object: nil, userInfo: dict)
    }

    class func getEventString(_ event: AdmanMessageEventType) -> String? {
        switch event {
            case .admanMessageEventFetchingInfo:
                return String(format: "AdmanMessageEventFetchingInfo (%ld)", event.rawValue)
            case .admanMessageEventPreloading:
                return String(format: "AdmanMessageEventPreloading (%ld)", event.rawValue)
            case .admanMessageEventNone:
                return String(format: "AdmanMessageEventNone (%ld)", event.rawValue)
            case .admanMessageEventError:
                return String(format: "AdmanMessageEventError (%ld)", event.rawValue)
            case .admanMessageEventReady:
                return String(format: "AdmanMessageEventReady (%ld)", event.rawValue)
            case .admanMessageEventStarted:
                return String(format: "AdmanMessageEventStarted (%ld)", event.rawValue)
            case .admanMessageEventStopped:
                return String(format: "AdmanMessageEventStopped (%ld)", event.rawValue)
            case .admanMessageEventCompleted:
                return String(format: "AdmanMessageEventCompleted (%ld)", event.rawValue)
            case .admanMessageEventSkipped:
                return String(format: "AdmanMessageEventSkipped (%ld)", event.rawValue)
            default:
                return "Unknown"
        }
    }
}

class NotificationAdmanPlayer: NSObject {
    private(set) var event: AdmanPlayerEventType!
    private(set) var userData: Any?

    init(_ event: AdmanPlayerEventType, andUserData userData: Any?) {
        super.init()
        self.event = event
        self.userData = userData
    }

    class func sendEvent(_ event: AdmanPlayerEventType) {
        NotificationAdmanPlayer.sendEvent(event, userData: nil)
    }

    class func sendEvent(_ event: AdmanPlayerEventType, userData: Any?) {
        let message = NotificationAdmanPlayer(event, andUserData: userData)
        let dict = [NOTIFICATION_ADMAN_PLAYER_KEY : message]
        NotificationCenter.default.post(name: NSNotification.Name(NOTIFICATION_ADMAN_PLAYER), object: nil, userInfo: dict)
    }

    class func getEventString(_ event: AdmanPlayerEventType) -> String? {
        switch event {
            case .admanPlayerEventPrepare:
                return String(format: "AdmanPlayerEventPrepare (%ld)", event.rawValue)
            case .admanPlayerEventReady:
                return String(format: "AdmanPlayerEventReady (%ld)", event.rawValue)
            case .admanPlayerEventFailed:
                return String(format: "AdmanPlayerEventFailed (%ld)", event.rawValue)
            case .admanPlayerEventPlaying:
                return String(format: "AdmanPlayerEventPlaying (%ld)", event.rawValue)
            case .admanPlayerEventPlay:
                return String(format: "AdmanPlayerEventPlay (%ld)", event.rawValue)
            case .admanPlayerEventPause:
                return String(format: "AdmanPlayerEventPause (%ld)", event.rawValue)
            case .admanPlayerEventProgress:
                return String(format: "AdmanPlayerEventProgress (%ld)", event.rawValue)
            case .admanPlayerEventComplete:
                return String(format: "AdmanPlayerEventComplete (%ld)", event.rawValue)
            case .admanPlayerEventCloseable:
                return String(format: "AdmanPlayerEventCloseable (%ld)", event.rawValue)
            case .admanPlayerEventSkippable:
                return String(format: "AdmanPlayerEventSkippable (%ld)", event.rawValue)
            default:
                return "Unknown"
        }
    }
}

class NotificationAdmanVoice: NSObject {
    var event: AdmanVoiceEventType!

    init(_ event: AdmanVoiceEventType) {
        super.init()
        self.event = event
    }

    class func sendEvent(_ event: AdmanVoiceEventType) {
        let message = NotificationAdmanVoice(event)
        let dict = [NOTIFICATION_ADMAN_VOICE_KEY : message]
        NotificationCenter.default.post(name: NSNotification.Name(NOTIFICATION_ADMAN_VOICE), object: nil, userInfo: dict)
    }

    class func getEventString(_ event: AdmanVoiceEventType) -> String? {
        switch event {
            case .admanVoiceEventResponsePlaying:
                return String(format: "AdmanVoiceEventResponsePlaying (%ld)", event.rawValue)
            case .admanVoiceEventInteractionStarted:
                return String(format: "AdmanVoiceEventInteractionStarted (%ld)", event.rawValue)
            case .admanVoiceEventInteractionEnded:
                return String(format: "AdmanVoiceEventInteractionEnded (%ld)", event.rawValue)
            case .admanVoiceEventError:
                return String(format: "AdmanVoiceEventError (%ld)", event.rawValue)
            case .admanVoiceEventRecognizerStarted:
                return String(format: "AdmanVoiceEventRecognizerStarted (%ld)", event.rawValue)
            case .admanVoiceEventRecognizerStopped:
                return String(format: "AdmanVoiceEventRecognizerStopped (%ld)", event.rawValue)
            default:
                return "Unknown"
        }
    }
}

class NotificationAdmanControl: NSObject {
    var event: AdmanControlEventType!
    private(set) var userData: Any?

    init(_ event: AdmanControlEventType, andUserData userData: Any?) {
        super.init()
        self.event = event
        self.userData = userData
    }

    class func sendEvent(_ event: AdmanControlEventType) {
        NotificationAdmanControl.sendEvent(event, userData: nil)
    }

    class func sendEvent(_ event: AdmanControlEventType, userData: Any?) {
        let message = NotificationAdmanControl(event, andUserData: userData)
        let dict = [NOTIFICATION_ADMAN_CONTROL_KEY : message]
        NotificationCenter.default.post(name: NSNotification.Name(NOTIFICATION_ADMAN_CONTROL), object: nil, userInfo: dict)
    }

    class func getEventString(_ event: AdmanControlEventType) -> String? {
        switch event {
            case .admanControlEventPrepare:
                return String(format: "AdmanControlEventPrepare (%ld)", event.rawValue)
            case .admanControlEventStart:
                return String(format: "AdmanControlEventStart (%ld)", event.rawValue)
            case .admanControlEventPause:
                return String(format: "AdmanControlEventPause (%ld)", event.rawValue)
            case .admanControlEventResume:
                return String(format: "AdmanControlEventResume (%ld)", event.rawValue)
            case .admanControlEventSkip:
                return String(format: "AdmanControlEventSkip (%ld)", event.rawValue)
            case .admanControlEventClick:
                return String(format: "AdmanControlEventClick (%ld)", event.rawValue)
            case .admanControlEventLink:
                return String(format: "AdmanControlEventLink (%ld)", event.rawValue)
            case .admanControlEventSwipeBack:
                return String(format: "AdmanControlEventSwipeBack (%ld)", event.rawValue)
            case .admanControlEventPositive:
                return String(format: "AdmanControlEventPositive (%ld)", event.rawValue)
            case .admanControlEventNegative:
                return String(format: "AdmanControlEventNegative (%ld)", event.rawValue)
            default:
                return "Unknown"
        }
    }
}

//}
