/// Copyright 2018 Instreamatic
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// MARK: Install via Package Manager: https://github.com/Matovsky/SwiftTryCatch

import AVFoundation
import Foundation
import UIKit
import SwiftTryCatch

enum AdmanPlayerItemType : Int {
    case admanPlayerItemAd
    case admanPlayerItemUnknown
    case admanPlayerItemReaction
    case admanPlayerItemMicOn
    case admanPlayerItemMicOff
    case admanPlayerItemInitial
}

private var preloadMode = AdmanPreloadModeProgressive as? AdmanPreloadMode

class AdmanPlayerItem: AVPlayerItem {
    private(set) var adID = 0
    var itemType: AdmanPlayerItemType!
    var isLoaded = false
    var isPlayed = false

    convenience init(string url: String?, adID: Int, itemType: AdmanPlayerItemType) {
        if let url1 = URL(string: url ?? "") {
            self.init(asset: AVAsset(url: url1), adID: adID, itemType: itemType)
        }
        return nil
    }

    init(asset: AVAsset?, adID: Int, itemType: AdmanPlayerItemType) {
        if let asset = asset {
            super.init(asset: asset)
        }
        self.adID = adID
        self.itemType = itemType
        isPlayed = false
        isLoaded = false
    }
}

protocol AdmanPlayerDelegate: NSObjectProtocol {
    func itemDidLoaded(_ adID: Int, itemType: AdmanPlayerItemType)
    func itemPlaybackDidFinished(_ adID: Int, itemType: AdmanPlayerItemType)
    func itemPlaybackDidFailed(_ adID: Int, itemType: AdmanPlayerItemType, err: Error?)
    func log(_ data: String?)
}

class AdmanPlayer: AVPlayer, AVAudioPlayerDelegate {
    var delegate: Any!
    var userData: NSObject?
    private(set) var isPlaying = false

    override init() {
        super.init()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(audioInterruptionSel(_:)),
            name: AVAudioSession.interruptionNotification,
            object: nil)
    }

    /// Async assets preloading from url (only mp3 links)
    class func preloadAsset(_ url: String?, onComplete fulfill: AdmanOnFulfill, onFail reject: AdmanOnFail) {
        DispatchQueue.global(qos: .default).async(execute: {
            var asset: AVURLAsset? = nil
            if let url1 = URL(string: url ?? "") {
                asset = AVURLAsset(url: url1, options: nil)
            }
            let keys = ["playable", "tracks", "duration"]
            asset?.loadValuesAsynchronously(forKeys: keys, completionHandler: {
                // make sure everything downloaded properly
                for thisKey in keys {
                    var error: Error? = nil
                    let keyStatus = asset?.statusOfValue(forKey: thisKey, error: &error)
                    if keyStatus == .failed {
                        DispatchQueue.main.sync(execute: {
                            reject(error)
                        })
                    } else {
                        DispatchQueue.main.sync(execute: {
                            fulfill(asset)
                        })
                    }
                }
            })
        })
    }

    class func setPreloadMode(_ mode: AdmanPreloadMode) {
        preloadMode = mode
    }

    class func getPreloadMode() -> AdmanPreloadMode {
        return preloadMode!
    }

    @objc func playbackFinished(_ notification: Notification?) {
        if (notification?.object as? AVPlayerItem) != currentItem {
            return
        }

        removeStateListeners()

        SwiftTryCatch.try({
            if let currentItem = currentItem {
                NotificationCenter.default.removeObserver(currentItem)
            }

            if observationInfo != nil {
                NotificationCenter.default.removeObserver(self)
            }
        }, catch: { exception in
            //NSLog(@"Failed to remove observer from AddmanPlayer: nothing to do");
        }, finallyBlock: {
        })

        itemPlayed()

    }

    func removeAllItems() {
        removeStateListeners()

        SwiftTryCatch.try({
            if let currentItem = currentItem {
                NotificationCenter.default.removeObserver(currentItem)
            }

            if observationInfo != nil {
                NotificationCenter.default.removeObserver(self)
            }
        }, catch: { exception in
        }, finallyBlock: {
        })

        replaceCurrentItem(with: nil)
    }

    func add(_ asset: AVAsset?, adID: Int, itemType: AdmanPlayerItemType) {
        let playerItem = AdmanPlayerItem(asset: asset, adID: adID, itemType: itemType)

        playerItem.addObserver(self, forKeyPath: "status", options: [.old, .new], context: nil)
        playerItem.addObserver(self, forKeyPath: "playbackBufferFull", options: [.old, .new], context: nil)
        playerItem.addObserver(self, forKeyPath: "playbackLikelyToKeepUp", options: [.old, .new], context: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(playbackFinished(_:)),
            name: .AVPlayerItemDidPlayToEndTime,
            object: nil)

        replaceCurrentItem(with: playerItem)
        if itemType != .admanPlayerItemMicOff && itemType != .admanPlayerItemMicOn {
            delegate?.log("Buffering \((playerItem.asset as? AVURLAsset)?.url.absoluteString ?? "")")
        }
    }

    func addUrl(_ url: String?, adID: Int, itemType: AdmanPlayerItemType) {
        if let url1 = URL(string: url ?? ""), let type = AdmanPlayerItemType(rawValue: itemType.rawValue) {
            add(AVAsset(url: url1), adID: adID, itemType: type)
        }
    }

    override func play() {
        if currentItem == nil {
            return
        }

        let logString = "Playing asset \((currentItem?.asset as? AVURLAsset)?.url.absoluteString ?? "")"
        delegate?.log(logString)

        DispatchQueue.main.async(execute: {
            UIApplication.shared.beginReceivingRemoteControlEvents()
        })
        isPlaying = true
        super.play()
    }

    override func observeValue(
        forKeyPath keyPath: String?,
        of object: Any?,
        change: [NSKeyValueChangeKey : Any]?,
        context: UnsafeMutableRawPointer?
    ) {
        if currentItem == nil {
            return
        }

        if keyPath == "status" {
            var status: AVPlayerItem.Status = .unknown
            let statusNumber = change?[.newKey] as? NSNumber
            if statusNumber is NSNumber {
                if let integerValue = AVPlayerItem.Status(rawValue: statusNumber?.intValue ?? 0) {
                    status = integerValue
                }
            }

            let item = currentItem as? AdmanPlayerItem
            switch status {
                case .readyToPlay, .unknown:
                    if item?.itemType == .admanPlayerItemMicOff || item?.itemType == .admanPlayerItemMicOn {
                        itemLoaded()
                    } else {
                        super.play()
                    }
                case .failed:
                    isPlaying = false
                    removeStateListeners()
                    if let itemType1 = item?.itemType {
                        delegate?.itemPlaybackDidFailed(item?.adID ?? 0, itemType: itemType1, err: currentItem?.error)
                    }
                    print("admanPlayer: failed to load audio")
                @unknown default:
                    break
            }
        } else if (keyPath == "playbackBufferFull") || (keyPath == "playbackLikelyToKeepUp") {
            let item = currentItem as? AdmanPlayerItem

            if item?.itemType == .admanPlayerItemMicOff || item?.itemType == .admanPlayerItemMicOn {
                return
            }

            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                super.pause()
            })
            itemLoaded()
        }

    }

    @objc func audioInterruptionSel(_ notification: Notification?) {
        if currentItem == nil {
            return
        }

        if !isPlaying {
            return
        }

        let t = notification?.userInfo?[AVAudioSessionInterruptionTypeKey] as? NSNumber
        switch t?.intValue ?? 0 {
            case AVAudioSession.InterruptionType.began.rawValue:
                super.pause()
            case AVAudioSession.InterruptionType.ended.rawValue:
                if (notification?.userInfo?[AVAudioSessionInterruptionOptionKey] as? NSNumber)?.intValue ?? 0 == AVAudioSession.InterruptionOptions.shouldResume.rawValue {
                    super.play()
                }
            default:
                break
        }
    }

    func removeStateListeners() {
        isPlaying = false
        SwiftTryCatch.try({
            if currentItem != nil && currentItem?.observationInfo != nil {
                currentItem?.removeObserver(self, forKeyPath: "status")
            }
        }, catch: { exception in
        }, finallyBlock: {
        })

        SwiftTryCatch.try({
            currentItem?.removeObserver(self, forKeyPath: "playbackLikelyToKeepUp")
        }, catch: { exception in
        }, finallyBlock: {
        })
        
        SwiftTryCatch.try({
            currentItem?.removeObserver(self, forKeyPath: "playbackBufferFull")
        }, catch: { exception in
        }, finallyBlock: {
        })
    }

    func itemLoaded() {
        let item = currentItem as? AdmanPlayerItem
        if !(item?.isLoaded ?? false) {
            item?.isLoaded = true
            removeStateListeners()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: { [self] in
                if let itemType1 = item?.itemType {
                    delegate?.itemDidLoaded(item?.adID ?? 0, itemType: itemType1)
                }
            })
        }
    }

    func itemPlayed() {
        let item = currentItem as? AdmanPlayerItem
        if !(item?.isPlayed ?? false) {
            item?.isPlayed = true
            isPlaying = false
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: { [self] in
                if let itemType1 = item?.itemType {
                    delegate?.itemPlaybackDidFinished(item?.adID ?? 0, itemType: itemType1)
                }
            })
        }
    }
}
