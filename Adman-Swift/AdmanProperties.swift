/// Copyright 2021 Instreamatic
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/// Copyright 2018 Instreamatic
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import AdSupport
import CoreLocation
import Foundation

class AdmanSettings: NSObject, CLLocationManagerDelegate {
    /// ID for advertisement
    private(set) var idfa = ""
    /// flag which is used to indicate if ad tracking was enabled on the device
    private(set) var isTrackingEnabled = false
    var useGeolocationTargeting = false
    var latitude: Float = 0.0
    var longitude: Float = 0.0
    /// Allow libAdman to setup sharedInstance of AVAudioSession for correct playback
    /// default: true
    var isAudioSessionSetupEnabled = false
    var debugMode = 0
    var dtmfAdsDetection = false
    /// Short radiostation name
    var dtmfStationKey: String?
    var siteId = 0
    var zoneId = 0
    var campaignId = 0
    var creativeId = 0
    var type: AdmanType?
    var region: AdmanRegion?
    var format: AdmanFormat?
    var maxAdBlockDuration = 0
    var maxAdsCount = 0
    var slot: AdmanAdSlot?
    /// override ip sended to ad server
    var clientIP: String?
    /// speech-to-text voice engine
    var backend: AdmanRecognitionBackend?
    var recognitionBackendLocation: AdmanRecognitionBackendLocation?
    var serverVoiceActivityDetection = false
    var clientVoiceActivityEnded = false
    /// default voice response delay value from VAST
    var voiceResponseDelay: Float = 0.0

    private var _serverTimeout = 0
    var serverTimeout: Int {
        get {
            _serverTimeout
        }
        set(delay) {
            _serverTimeout = delay
        }
    }
    /// user defined response delay value
    var customVoiceResponseDelay: Float = 0.0
    var customAdResponseTime: Float = 0.0
    var siteVariables: [String : String] = [:]
    var customAdServer: String?
    var requestURL: String?
    var lastFetchTime: Date?
    private var clManager: CLLocationManager?

    func setServerTimeout(_ delay: Int) {
    }

    override init() {
        super.init()
        let adIdentManager = ASIdentifierManager.shared()
        idfa = adIdentManager.advertisingIdentifier.uuidString
        isTrackingEnabled = adIdentManager.isAdvertisingTrackingEnabled
        isAudioSessionSetupEnabled = true

        format = AdmanFormatAny
        type = AdmanTypeAny
        slot = AdmanAdSlotDefault
        backend = AdmanRecognitionBackendAuto
        recognitionBackendLocation = AdmanRecognitionBackendLocationAuto
        serverVoiceActivityDetection = false
        useGeolocationTargeting = false
        voiceResponseDelay = -1.0
        customVoiceResponseDelay = 0
        customAdResponseTime = 0
        serverTimeout = 4
        siteVariables = [:]

        longitude = -1
        latitude = 0
    }

    func setPreview(_ mode: AdmanDebugMode) {
        debugMode = NSNumber(value: Int(mode))
    }

    func getResponseDelay() -> Float {
        var delay = voiceResponseDelay
        if customVoiceResponseDelay != 0 {
            delay = customVoiceResponseDelay
        }
        return delay
    }

    func setResponseDelay(_ delay: Float) {
        customVoiceResponseDelay = delay * 1.0
    }

    func disableAudioSessionSetup() {
        isAudioSessionSetupEnabled = false
    }

    func setResponseTime(_ duration: Float) {
        customAdResponseTime = duration
    }

    func getResponseTime() -> Float {
        return customAdResponseTime
    }

    func enableGeolocationTargeting() {
        if useGeolocationTargeting || !useGeolocationTargeting {
            return
        }

        clManager = CLLocationManager()
        clManager?.delegate = self

        switch CLLocationManager.authorizationStatus {
            case .denied, .restricted:
                break
            case .notDetermined:
                clManager?.requestAlwaysAuthorization()
            case .authorizedAlways, .authorizedWhenInUse:
                useGeolocationTargeting = true
                clManager?.startUpdatingLocation()
            default:
                break
        }
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            useGeolocationTargeting = true
            manager.startUpdatingLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        longitude = Float(locations.last?.coordinate.longitude ?? 0.0)
        latitude = Float(locations.last?.coordinate.latitude ?? 0.0)
        manager.stopUpdatingLocation()
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if longitude == -1 && latitude == 0 {
            useGeolocationTargeting = false
        }
    }

    func setSiteVariable(_ key: String, value: String) {
        siteVariables[key] = value
    }
}
