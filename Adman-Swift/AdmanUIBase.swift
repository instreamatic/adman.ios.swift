/// Copyright 2019 Instreamatic
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation
import SafariServices
import UIKit

class AdmanUIBase: UIViewController, SFSafariViewControllerDelegate {
    var rootVC: UIViewController?
    var microphoneStatus: UIView?
    var voiceRequestAnimation: UIImageView?
    var bannerView: UIImageView?
    var closeView: UIImageView?
    var playView: UIImageView?
    var rewindView: UIImageView?
    var remainingTime: UILabel?
    var intentLabel: UILabel?
    var positiveView: UIImageView?
    var negativeView: UIImageView?
    private var ad: AdmanBannerWrapper!

    override init() {
        super.init()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(admanPlayerNotification(_:)),
            name: NOTIFICATION_ADMAN_PLAYER,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(admanControlNotification(_:)),
            name: NOTIFICATION_ADMAN_CONTROL,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(admanVoiceNotification(_:)),
            name: NOTIFICATION_ADMAN_VOICE,
            object: nil)
    }

    class func add(_ sel: Selector, for view: UIView, withTarget delegate: Any) {
        let tapper = UITapGestureRecognizer(target: delegate, action: sel)
        tapper.numberOfTapsRequired = 1
        tapper.numberOfTouchesRequired = 1
        tapper.delegate = delegate as? UIGestureRecognizerDelegate
        view.addGestureRecognizer(tapper)
        view.isUserInteractionEnabled = true
    }

    class func loadImage(from base64: String?) -> UIImage? {
        if let url = URL(string: base64 ?? ""), let data = Data(contentsOf: url) {
            return UIImage(data: data)
        }
        return nil
    }

    class func setVisible(_ visibility: Bool, view listView: [UIView]?) {
        for viewItem in listView ?? [] {
            if viewItem == nil {
                continue
            }
            viewItem.isHidden = !visibility
        }
    }

    func initBaseView() {
        if rootVC == nil {
            return
        }

        // transparent background color
        view.backgroundColor = UIColor.black
        let bounds = UIScreen.main.bounds
        view.frame = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.width, height: bounds.height)

        rootVC?.view.addSubview(view)
        rootVC?.addChild(self)
        didMove(toParent: rootVC)

        if rootVC?.navigationController != nil {
            let gesture = rootVC?.navigationController?.view.gestureRecognizers?.first as? UIScreenEdgePanGestureRecognizer
            if let gesture = gesture {
                gesture.addTarget(self, action: #selector(onSwipeBack(_:)))
            }
        }
        initBaseComponents()

        // Timer label
        if remainingTime != nil {
            remainingTime?.textColor = UIColor.white
            remainingTime?.backgroundColor = UIColor.clear
            if let remainingTime = remainingTime {
                view.addSubview(remainingTime)
            }
            remainingTime?.font = UIFont(name: "Arial", size: 25)
            if ad != nil {
                setTime(ad.duration)
            }
        }

        // play button
        if playView != nil {
            if let playView = playView {
                AdmanUIBase.add(#selector(playStateChanged(_:)), for: playView, withTarget: self)
                view.addSubview(playView)
            }
        }

        // rewind button
        if rewindView != nil {
            if let rewindView = rewindView {
                AdmanUIBase.add(#selector(playbackRewindEvent(_:)), for: rewindView, withTarget: self)
                view.addSubview(rewindView)
            }
        }

        if ad != nil && ad.isVoiceAd() {
            initVoiceView()
        }

        // Banner image
        if let bannerView = bannerView {
            view.addSubview(bannerView)
        }
        if ad.url {
            update(ad)
        }

        // Close button
        if closeView != nil {
            if let closeView = closeView {
                AdmanUIBase.add(#selector(onClose(_:)), for: closeView, withTarget: self)
                view.addSubview(closeView)
            }
            closeView?.isHidden = true
        }

        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(orientationChangedEvent(_:)),
            name: UIDevice.orientationDidChangeNotification,
            object: UIDevice.current)

        if microphoneStatus != nil {
            microphoneStatus?.backgroundColor = UIColor.red
            if let microphoneStatus = microphoneStatus {
                view.addSubview(microphoneStatus)
            }
            microphoneStatus?.isHidden = true
        }
    }

    func initBaseComponents() {
        if remainingTime == nil {
            remainingTime = UILabel()
        }

        if playView == nil {
            playView = UIImageView(image: AdmanUIBase.loadImage(from: PlayButtonString))
        }

        if rewindView == nil {
            rewindView = UIImageView(image: AdmanUIBase.loadImage(from: BackButtonString))
        }

        if bannerView == nil {
            bannerView = UIImageView(frame: CGRect(x: 0, y: 0, width: 640, height: 640))
        }

        if closeView == nil {
            closeView = UIImageView(image: AdmanUIBase.loadImage(from: CloseButtonString))
        }

        if microphoneStatus == nil {
            microphoneStatus = UIView(frame: CGRect(x: 0, y: 0, width: rootVC?.view.frame.size.width ?? 0.0, height: UIApplication.shared.statusBarFrame.size.height))
        }
    }

    func initVoiceView() {
        var img: UIImage? = nil
        var animation = [AnyHashable](repeating: 0, count: 41)
        for i in 1...40 {
            if i < 10 {
                img = UIImage(named: String(format: "loading_1000%i.gif", i))
            } else {
                img = UIImage(named: String(format: "loading_100%i.gif", i))
            }
            if let img = img {
                animation.append(img)
            }
        }

        initVoiceComponents()

        if voiceRequestAnimation != nil {
            voiceRequestAnimation?.animationImages = animation as? [UIImage]
            voiceRequestAnimation?.animationDuration = 1.0
            voiceRequestAnimation?.animationRepeatCount = 0
            voiceRequestAnimation?.startAnimating()
            if let voiceRequestAnimation = voiceRequestAnimation {
                view.addSubview(voiceRequestAnimation)
            }
            voiceRequestAnimation?.isHidden = true
        }

        if intentLabel != nil {
            let locale = ad.responseLang ?? NSLocale.preferredLanguages.first
            if (locale as NSString?)?.range(of: "ru").location != NSNotFound {
                intentLabel?.text = "Хотите узнать больше?"
                intentLabel?.font = UIFont(name: "Arial", size: 17)
            } else if (locale as NSString?)?.range(of: "ja").location != NSNotFound {
                intentLabel?.text = "興味がありますか"
                intentLabel?.font = UIFont(name: "Arial", size: 23)
            } else {
                intentLabel?.text = "Interested?"
                intentLabel?.font = UIFont(name: "Arial", size: 27)
            }
            intentLabel?.textColor = UIColor.white
            intentLabel?.backgroundColor = UIColor.clear
            if let intentLabel = intentLabel {
                view.addSubview(intentLabel)
            }
            intentLabel?.isHidden = true
        }

        // negative intent
        if negativeView != nil {
            if let negativeView = negativeView {
                AdmanUIBase.add(#selector(negativeIntent(_:)), for: negativeView, withTarget: self)
                view.addSubview(negativeView)
            }
            negativeView?.isHidden = true
        }

        // positive intent
        if positiveView != nil {
            if let positiveView = positiveView {
                AdmanUIBase.add(#selector(positiveIntent(_:)), for: positiveView, withTarget: self)
                view.addSubview(positiveView)
            }
            positiveView?.isHidden = true
        }
    }

    func initVoiceComponents() {
        let img = UIImage(named: "loading_10001.gif")

        if voiceRequestAnimation == nil {
            voiceRequestAnimation = UIImageView(frame: CGRect(x: 0, y: 0, width: img?.size.width ?? 0.0, height: img?.size.height ?? 0.0))
        }

        if intentLabel == nil {
            intentLabel = UILabel()
        }

        let locale = ad.responseLang ?? NSLocale.preferredLanguages.first
        var buttonLocal = NegativeButtonString as? String
        if (locale as NSString?)?.range(of: "ru").location != NSNotFound {
            buttonLocal = NegativeButtonStringRu
        } else if (locale as NSString?)?.range(of: "ja").location != NSNotFound {
            buttonLocal = NegativeButtonStringJa
        }

        if negativeView == nil {
            negativeView = UIImageView(image: AdmanUIBase.loadImage(from: buttonLocal))
        }

        buttonLocal = PositiveButtonString
        if (locale as NSString?)?.range(of: "ru").location != NSNotFound {
            buttonLocal = PositiveButtonStringRu
        } else if (locale as NSString?)?.range(of: "ja").location != NSNotFound {
            buttonLocal = PositiveButtonStringJa
        }

        if positiveView == nil {
            positiveView = UIImageView(image: AdmanUIBase.loadImage(from: buttonLocal))
        }
    }

    func update(_ ad: AdmanBannerWrapper?) {
        self.ad = ad

        if self.ad != nil && ad?.isVoiceAd() && intentLabel == nil {
            initVoiceView()
        }

        if ad != nil && ad?.bannerImage != nil {
            bannerView?.image = ad?.bannerImage
            if let bannerView = bannerView {
                AdmanUIBase.add(#selector(bannerTapped(_:)), for: bannerView, withTarget: self)
            }
            bannerView?.isHidden = false
            rotateByOrientation()
        } else {
            bannerView?.image = nil
            bannerView?.isHidden = true
        }

        closeView?.isHidden = true
    }

    // MARK: common methods

    func show(_ rootVC: UIViewController?) {
        self.rootVC = rootVC

        initBaseView()
        self.rootVC?.view.addSubview(view)
        rotateByOrientation()
    }

    func hide() {
        if !Thread.isMainThread {
            DispatchQueue.main.async(execute: { [self] in
                view.removeFromSuperview()
                removeFromParent()
            })
        } else {
            view.removeFromSuperview()
            removeFromParent()
        }
        intentLabel = nil
        ad = nil
    }

    func setTime(_ time: Int) {
        if remainingTime == nil {
            return
        }

        let v = (time < 1) ? 0 : time
        if time > 9 {
            remainingTime?.text = String(format: "00:%li", v)
        } else {
            remainingTime?.text = String(format: "00:0%li", v)
        }
    }

    func showCloseButton() {
        if ad.isVoiceAd() {
            return
        }

        if let closeView = closeView {
            closeView.isHidden = false
        }
    }

    func showPauseButton(_ tapGestureRecognizer: UITapGestureRecognizer?) {
        if tapGestureRecognizer == nil {
            if let firstObject = playView?.gestureRecognizers?.first {
                playView?.removeGestureRecognizer(firstObject)
            }
        } else {
            if let tapGestureRecognizer = tapGestureRecognizer {
                playView?.removeGestureRecognizer(tapGestureRecognizer)
            }
        }

        playView?.image = AdmanUIBase.loadImage(from: PauseButtonString)
        if let playView = playView {
            AdmanUIBase.add(#selector(pauseStateChanged(_:)), for: playView, withTarget: self)
        }
    }

    func showPlayButton(_ tapGestureRecognizer: UITapGestureRecognizer?) {
        if tapGestureRecognizer == nil {
            if let firstObject = playView?.gestureRecognizers?.first {
                playView?.removeGestureRecognizer(firstObject)
            }
        } else {
            if let tapGestureRecognizer = tapGestureRecognizer {
                playView?.removeGestureRecognizer(tapGestureRecognizer)
            }
        }

        playView?.image = AdmanUIBase.loadImage(from: PlayButtonString)
        if let tapGestureRecognizer = tapGestureRecognizer {
            playView?.removeGestureRecognizer(tapGestureRecognizer)
        }
        if let playView = playView {
            AdmanUIBase.add(#selector(playStateChanged(_:)), for: playView, withTarget: self)
        }
    }

    @objc func orientationChangedEvent(_ notify: Notification?) {
        rotateByOrientation()
    }

    func rotateByOrientation() {
        let toInterfaceOrientation = UIApplication.shared.statusBarOrientation
        if toInterfaceOrientation.isPortrait || ad.isVoiceAd() {
            rotateVertical()
        } else if toInterfaceOrientation.isLandscape {
            rotateHorizontal()
        }
    }

    func rotateVertical() {
        let bounds = UIScreen.main.bounds

        let screenWidth = Int(bounds.width)
        let screenHeight = Int(bounds.height)
        let spacerSize = screenWidth * Int(UIControlsSpacerRatio)
        let screenCenter = CGPoint(x: CGFloat(screenWidth / 2), y: CGFloat(screenHeight / 2))

        // banner
        if ad.url {
            var side = screenWidth - spacerSize
            if ad.isVoiceAd() {
                side = screenWidth
            }
            let centerPoint = CGPoint(x: CGFloat(side / 2), y: CGFloat(side / 2))
            let bannerPoint = CGRect(x: screenCenter.x - centerPoint.x, y: screenCenter.y - centerPoint.y, width: CGFloat(side), height: CGFloat(side))
            if ad.isVoiceAd() {
                bannerPoint.origin.y -= CGFloat(spacerSize * 2)
            }
            bannerView?.frame = bannerPoint
        }

        var mainControlsRect: CGRect
        // play/pause buttons position
        mainControlsRect = CGRect(
            x: screenCenter.x - (playView?.image?.size.width ?? 0.0) / 2,
            y: CGFloat(screenHeight) - (playView?.image?.size.height ?? 0.0) * 2,
            width: playView?.image?.size.width ?? 0.0,
            height: playView?.image?.size.height ?? 0.0)
        playView?.frame = mainControlsRect

        // back button
        let rewind = CGRect(
            x: mainControlsRect.origin.x,
            y: mainControlsRect.origin.y,
            width: rewindView?.image?.size.width ?? 0.0,
            height: rewindView?.image?.size.height ?? 0.0)
        rewind.origin.x -= (rewindView?.image?.size.width ?? 0.0) + (playView?.image?.size.width ?? 0.0) * 1.2
        rewindView?.frame = rewind

        if !ad.isVoiceAd() {
            // close
            let close = CGRect(
                x: CGFloat(screenWidth) - (closeView?.image?.size.width ?? 0.0) * 1.6,
                y: UIApplication.shared.statusBarFrame.size.height * 1.1,
                width: closeView?.image?.size.width ?? 0.0,
                height: closeView?.image?.size.height ?? 0.0)
            closeView?.frame = close
        } else {
            let buttonWidth = screenWidth / 2 - spacerSize * 3
            let x = screenWidth / 2
            // x, y, sizeX, sizeY
            mainControlsRect = CGRect(
                x: CGFloat(x - spacerSize - buttonWidth),
                y: Double(screenHeight - spacerSize) - Double(buttonWidth) / 2.19,
                width: CGFloat(buttonWidth),
                height: Double(buttonWidth) / 2.19)
            negativeView?.frame = mainControlsRect
            closeView?.frame = CGRect(x: CGFloat(screenWidth - spacerSize - 16), y: (bannerView?.frame.origin.y ?? 0.0) + CGFloat(spacerSize), width: 16, height: 16)

            let locale = ad.responseLang ?? NSLocale.preferredLanguages.first
            if (locale as NSString?)?.range(of: "ru").location != NSNotFound || (locale as NSString?)?.range(of: "ja").location != NSNotFound {
                intentLabel?.frame = CGRect(x: CGFloat(screenWidth / 2 - 90), y: (negativeView?.frame.origin.y ?? 0.0) - 32 - CGFloat(spacerSize * 2), width: 190, height: 32)
            } else {
                intentLabel?.frame = CGRect(x: CGFloat(screenWidth / 2 - 70), y: (negativeView?.frame.origin.y ?? 0.0) - 32 - CGFloat(spacerSize * 2), width: 140, height: 32)
            }

            mainControlsRect.origin.x = CGFloat(x + spacerSize)
            positiveView?.frame = mainControlsRect
        }

        // timer
        if !ad.isVoiceAd() {
            mainControlsRect.origin.x += (playView?.image?.size.width ?? 0.0) * 2
        } else {
            mainControlsRect.origin.y = CGFloat(spacerSize * 2)
            mainControlsRect.origin.x = CGFloat(screenWidth / 2 - 25)
            voiceRequestAnimation?.frame = CGRect(
                x: CGFloat(screenWidth / 2) - (voiceRequestAnimation?.frame.size.width ?? 0.0) / 2 + CGFloat(spacerSize / 3),
                y: mainControlsRect.origin.y + CGFloat(spacerSize * 2) + (remainingTime?.frame.size.height ?? 0.0),
                width: voiceRequestAnimation?.frame.size.width ?? 0.0,
                height: voiceRequestAnimation?.frame.size.height ?? 0.0)
        }
        mainControlsRect.size = CGSize(width: 100, height: 32)
        remainingTime?.frame = mainControlsRect
    }

    func rotateHorizontal() {
        let bounds = UIScreen.main.bounds

        let screenWidth = Int(bounds.width)
        let screenHeight = Int(bounds.height)
        let spacing = screenHeight * (Int(UIControlsSpacerRatio) * 2)

        // banner
        let side = screenHeight - spacing
        let bannerPoint = CGRect(x: CGFloat(0 + spacing / 2), y: CGFloat(0 + spacing / 2), width: CGFloat(side), height: CGFloat(side))
        if ad.url {
            if CGFloat(screenHeight / 2) < bannerPoint.size.width {
                let d = Int(((bannerPoint.size.width - CGFloat(screenWidth / 2)) / 2))
                bannerPoint.size.width -= CGFloat(d)
                bannerPoint.size.height = bannerPoint.size.width
                bannerPoint.origin.y += CGFloat(d / 2)
            }
            bannerView?.frame = bannerPoint
        }

        var control: CGRect
        if !ad.isVoiceAd() {
            // play/pause
            var xPos: Int
            if ad.url {
                xPos = Int(CGFloat(screenWidth) - (CGFloat(screenWidth) - (bannerPoint.size.width + CGFloat(spacing))) / 1.8)
            } else {
                xPos = screenWidth / 2
            }
            control = CGRect(
                x: CGFloat(xPos),
                y: CGFloat(screenHeight) - (playView?.image?.size.height ?? 0.0) * 2,
                width: playView?.image?.size.width ?? 0.0,
                height: playView?.image?.size.height ?? 0.0)
            playView?.frame = control

            // back button
            let rewind = CGRect(x: control.origin.x, y: control.origin.y + 4, width: rewindView?.image?.size.width ?? 0.0, height: rewindView?.image?.size.height ?? 0.0)
            rewind.origin.x -= (rewindView?.image?.size.width ?? 0.0) + (rewindView?.image?.size.width ?? 0.0) * 1.2
            rewindView?.frame = rewind
        }

        // close button
        let r = CGRect(
            x: CGFloat(screenWidth) - (closeView?.image?.size.width ?? 0.0) * 1.7,
            y: UIApplication.shared.statusBarFrame.size.height * 1.1,
            width: closeView?.frame.size.width ?? 0.0,
            height: closeView?.frame.size.height ?? 0.0)
        closeView?.frame = r

        // timer
        if !ad.isVoiceAd() {
            control.origin.x += (playView?.image?.size.width ?? 0.0) * 2
        }
        control.size = CGSize(width: 100, height: 32)
        remainingTime?.frame = control

    }

    // MARK: UI events

    @objc func positiveIntent(_ tapGestureRecognizer: UITapGestureRecognizer?) {
        NotificationAdmanControl.sendEvent(AdmanControlEventPositive)
    }

    @objc func negativeIntent(_ tapGestureRecognizer: UITapGestureRecognizer?) {
        NotificationAdmanControl.sendEvent(AdmanControlEventNegative)
    }

    @objc func bannerTapped(_ tapGestureRecognizer: UITapGestureRecognizer?) {
        if ad.isClickable {
            NotificationAdmanControl.sendEvent(AdmanControlEventClick)
        }
    }

    @objc func playStateChanged(_ tapGestureRecognizer: UITapGestureRecognizer?) {
        NotificationAdmanControl.sendEvent(AdmanControlEventStart)
        showPauseButton(tapGestureRecognizer)
    }

    @objc func pauseStateChanged(_ tapGestureRecognizer: UITapGestureRecognizer?) {
        NotificationAdmanControl.sendEvent(AdmanControlEventPause)
        showPlayButton(tapGestureRecognizer)
    }

    @objc func playbackRewindEvent(_ tapGestureRecognizer: UITapGestureRecognizer?) {
        NotificationAdmanControl.sendEvent(AdmanControlEventResume)
    }

    @objc func onSwipeBack(_ sender: Any?) {
        if rootVC == nil {
            return
        }
        hide()
        NotificationAdmanControl.sendEvent(AdmanControlEventSwipeBack)
    }

    @objc func onClose(_ tapGestureRecognizer: UITapGestureRecognizer?) {
        if ad.isVoiceAd() {
            AdmanUIBase.setVisible(false, view: [closeView, bannerView].compactMap { $0 })
        } else {
            NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: UIDevice.current)
            NotificationAdmanControl.sendEvent(AdmanControlEventSkip)
            hide()
        }
    }

    // MARK: Adman notifications

    @objc func admanPlayerNotification(_ notification: Notification?) {
        let dict = notification?.userInfo
        let message = dict?[NOTIFICATION_ADMAN_PLAYER_KEY] as? NotificationAdmanPlayer
        if message == nil {
            return
        }

        switch message?.event {
            case AdmanPlayerEventPlaying:
                print("\(TAG) triggerActionAdmanPlayer: \(NotificationAdmanPlayer.getEventString(message?.event))")
                DispatchQueue.main.async(execute: { [self] in
                    update(message?.userData)
                    showPauseButton(nil)
                    AdmanUIBase.setVisible(
                        false,
                        view: [negativeView, positiveView, intentLabel, microphoneStatus, voiceRequestAnimation].compactMap { $0 })
                })
            case AdmanPlayerEventReady:
                break
            case AdmanPlayerEventPlay:
                showPauseButton(nil)
            case AdmanPlayerEventPause:
                showPlayButton(nil)
            case AdmanPlayerEventProgress:
                let estimetedTime = message?.userData
                setTime(estimetedTime?.intValue ?? 0)

                if estimetedTime?.intValue ?? 0 < 4 {
                    showCloseButton()
                }
            case AdmanPlayerEventComplete:
                hide()
            default:
                break
        }
    }

    @objc func admanVoiceNotification(_ notification: Notification?) {
        let dict = notification?.userInfo
        let message = dict?[NOTIFICATION_ADMAN_VOICE_KEY] as? NotificationAdmanVoice
        if message == nil {
            return
        }

        print("\(TAG) triggerActionAdmanVoice: \(NotificationAdmanVoice.getEventString(message?.event))")
        switch message?.event {
            case AdmanVoiceEventError, AdmanVoiceEventResponsePlaying:
                DispatchQueue.main.async(execute: { [self] in
                    voiceRequestAnimation?.isHidden = true
                })
            case AdmanVoiceEventInteractionStarted:
                break
            case AdmanVoiceEventInteractionEnded:
                break
            case AdmanVoiceEventRecognizerStarted:
                DispatchQueue.main.async(execute: { [self] in
                    playView?.isHidden = true
                    rewindView?.isHidden = true

                    AdmanUIBase.setVisible(
                        true,
                        view: [microphoneStatus].compactMap { $0 })
                    if ad.showControls {
                        AdmanUIBase.setVisible(
                            true,
                            view: [intentLabel, positiveView, negativeView].compactMap { $0 })
                    }
                })
            case AdmanVoiceEventRecognizerStopped:
                DispatchQueue.main.async(execute: { [self] in
                    playView?.isHidden = false
                    rewindView?.isHidden = false
                    voiceRequestAnimation?.isHidden = false

                    AdmanUIBase.setVisible(
                        false,
                        view: [intentLabel, positiveView, negativeView, microphoneStatus].compactMap { $0 })
                })
            default:
                break
        }
    }

    @objc func admanControlNotification(_ notification: Notification?) {
        let dict = notification?.userInfo
        let message = dict?[NOTIFICATION_ADMAN_CONTROL_KEY] as? NotificationAdmanControl
        if message == nil {
            return
        }

        switch message?.event {
            case AdmanControlEventLink:
                var svc: SFSafariViewController? = nil
                if let userData = message?.userData {
                    svc = SFSafariViewController(url: userData)
                }
                if let svc = svc {
                    rootVC?.present(svc, animated: true)
                }
            default:
                return
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

let TAG = "AdmanUI"
