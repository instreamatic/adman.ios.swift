/// Copyright 2018 Instreamatic
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.

protocol AdmanVastParserDelegate: NSObjectProtocol {
    func creativeDidParsed(_ ads: [AdmanBanner])
    func creativeDidParseRedirect(_ redirectURL: String) -> String
    func creativeDidFailed(_ error: Error)
}

class AdmanVastParser: NSObject {
    var delegate: Any!
    var adsLoaded = 0
    private var adsList: [AnyHashable]?
    private var operation: URLSessionTask?

    func parse(_ url: String?, params: [AnyHashable : Any]?) {

        startLoadingAds(
            fromUrl: url,
            withParams: params,
            onComplete: { [self] adslist in
                if let adslist = adslist as? [AdmanBanner] {
                    delegate?.creativeDidParsed(adslist)
                }
            },
            onFail: { [self] err in
                if let err = err {
                    delegate?.creativeDidFailed(err)
                }
            })
    }

    func startLoadingAds(
//        fromUrl url: ,
        withParams params: [AnyHashable : Any]?,
        onComplete fulfill: AdmanOnFulfill,
        onFail fail: AdmanOnFail
    ) {
        adsList = nil
        adsLoaded = 0

        DispatchQueue.global(qos: .default).async(execute: { [self] in
            operation = AdmanVastParser.request(url, params: params, on: { [self] responseObject, response, error in
                print("libAdmam.xmlparser: Data recived from ad server")
                let httpResponse = response as? HTTPURLResponse
                if httpResponse?.statusCode != 200 {
                    fail(error)
                    return
                }

                var aXMLParser: XMLParser? = nil
                if let responseObject = responseObject {
                    aXMLParser = XMLParser(data: responseObject)
                }
                aXMLParser?.shouldProcessNamespaces = true
                let parser = AdmanVastParserDelegate()
                aXMLParser.delegate = parser
                aXMLParser?.parse()

                if parser.adsList.count() > 0 {
                    let sortDesc = NSSortDescriptor(key: "sequence", ascending: true)
                    let sortDescriptors = [sortDesc]
                    adsList = parser.adsList.sortedArray(using: sortDescriptors) as? [AnyHashable]
                    for item in adsList ?? [] {
                        guard let item = item as? AdmanBannerWrapper else {
                            continue
                        }
                        if item.redirectUrl != nil {
                            parseWrapper(item, onComplete: fulfill)
                        } else {
                            adLoaded(item, onComplete: fulfill)
                        }
                    }
                } else {
                    DispatchQueue.main.sync(execute: {
                        fulfill(parser.adsList)
                    })
                }
            })
            operation?.resume()
        })
    }

    func adLoaded(_ ad: AdmanBannerWrapper?, onComplete fulfill: AdmanOnFulfill) {
        adsLoaded += 1

        if adsLoaded == (adsList?.count ?? 0) {
            fulfill(adsList)
            adsList = nil
        }
    }

    func cancel() {
        operation?.cancel()
    }

    func parseWrapper(_ ad: AdmanBannerWrapper?, onComplete fulfill: AdmanOnFulfill) {
        ad?.redirectUrl = self.delegate?.creativeDidParseRedirect(ad?.redirectUrl ?? "")
        if let redirectUrl = ad?.redirectUrl {
            print("Start parsing redirect to \(redirectUrl)")
        }
        operation = AdmanVastParser.request(ad?.redirectUrl, params: nil, on: { [self] responseObject, response, err in
            if err != nil {
                ad?.url = nil
                ad?.sourceUrl = nil
                ad?.redirectUrl = nil
                print("Failed to parse redirect")
                adLoaded(ad, onComplete: fulfill)
                return
            }
            var aXMLParser: XMLParser? = nil
            if let responseObject = responseObject {
                aXMLParser = XMLParser(data: responseObject)
            }
            aXMLParser?.shouldProcessNamespaces = true
            let delegate = AdmanVastParserDelegate()
            aXMLParser?.delegate = delegate

            ad?.redirectUrl = nil
            delegate.wrappedAd = ad
            aXMLParser?.parse()
            if ad?.redirectUrl {
                parseWrapper(ad, onComplete: fulfill)
            } else {
                adLoaded(ad, onComplete: fulfill)
            }
            print("Redirect parsed succefully")
        })
    }

    class func request(_ url: String?, params: [AnyHashable : Any]?, on onComplete: AdmanSessionComplete) -> URLSessionTask? {
        let urlComponent = NSURLComponents(string: url ?? "")
        var queryItems = [AnyHashable](repeating: 0, count: params?.count ?? 0)
        for key in params ?? [:] {
            guard let key = key as? String else {
                continue
            }
            queryItems.append(NSURLQueryItem(name: key, value: params?[key] as? String))
        }
        urlComponent?.queryItems = queryItems as? [URLQueryItem]

        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 7.0

        if let URL = urlComponent?.url {
            return URLSession(configuration: sessionConfig).dataTask(with: URL, completionHandler: onComplete)
        }
        return nil
    }
}
