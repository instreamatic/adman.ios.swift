/// Copyright 2018 Instreamatic
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/// Copyright 2021 Instreamatic
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import AVFoundation
import Foundation

private var defaultImageSize = "640x640"
private var defaultAudioFormat = "mp3"
private var defaultBitrate = 128

enum AdmanVastParserProp : Int {
        case response
        case keyword
    }


class AdmanBannerWrapper: AdmanBanner {
    var crossServerAdID: String?
    var responseDelay = 0
    var responseURL: String?
    var responseLang: String?
    var redirectUrl: String?
    var sequence = 0
}

class AdmanVastParserDelegate: NSObject, NSXMLParserDelegate {
    var adsList: [AdmanBannerWrapper] = []
    var wrappedAd: AdmanBannerWrapper?
    private var attributes: [[AnyHashable : Any]]?
    private var elementName: String?
    private var outString: String?
    private var response: AdmanInteraction?
    private var staticResourceSize: String?
    private var rootTracking = false
    private var companionTracking: [String : [String]]?

    func parserDidStartDocument(_ parser: XMLParser) {
        adsList = []
        attributes = [AnyHashable](repeating: 0, count: 5)
        rootTracking = true

        if wrappedAd != nil {
            adsList.append(wrappedAd)
        }
        if PARSER_DEBUG_ENABLED {
            print("VAST parser started")
        }
    }

    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        self.elementName = qName
        if PARSER_DEBUG_ENABLED {
            print("Processing element: \(elementName)")
        }
        if let attributeDict = attributeDict {
            attributes?.add(attributeDict)
        } else {
            attributes?.add([:])
        }

        let handler = NSSelectorFromString("xml\(elementName)Start:")
        if responds(to: handler) {
        }
        method(for: handler)
        self
        handler
        attributeDict

        outString = ""
    }

    func xmlCompanionStart(_ attributeDict: [AnyHashable : Any]?) {
        rootTracking = false
        companionTracking = [AnyHashable : Any](minimumCapacity: 4) as? [String : [String]]
        if let object = attributeDict?["height"], let object1 = attributeDict?["width"] {
            staticResourceSize = "\(object)x\(object1)"
        }
    }

    func xmlTrackingStart(_ attributeDict: [AnyHashable : Any]?) {
        let event = attributeDict?["event"] as? String

        if !rootTracking && companionTracking?[event ?? ""] == nil {
            companionTracking?[event] = [AnyHashable](repeating: 0, count: 2)
        }
    }

    func xmlResponseStart(_ attributeDict: [AnyHashable : Any]?) {
        let action = AdmanInteraction(action: attributeDict?["action"], andType: attributeDict?["type"])
        action.priority = (attributeDict?["priority"] as? NSNumber)?.intValue ?? 0

        var response = adsList.last?.responses[attributeDict?["action"]] as? [AnyHashable]
        if let response = response {
            response.append(action)
        } else {
            if let aAttributeDict = attributeDict?["action"] {
                adsList.last?.responses[aAttributeDict] = [action]
            }
        }
        if let aAttributeDict = attributeDict?["action"] {
            self.response = adsList.last?.responses[aAttributeDict]?.last as? AdmanInteraction
        }
    }

    func xmlResponsesStart(_ attributeDict: [AnyHashable : Any]?) {
        adsList.last?.responses = [AnyHashable : Any](minimumCapacity: 6)
    }

    func xmlValueStart(_ attributeDict: [AnyHashable : Any]?) {
        if attributeDict == nil {
            return
        }

        if attributeDict?["type"] == "tracking_events" {
            return
        }
        if attributeDict?["type"] == "media" {
            return
        }

        response?.values.append(AdmanResponseValue(type: attributeDict?["type"], andData: nil))
    }

    func xmlAdStart(_ attributeDict: [AnyHashable : Any]?) {
        if wrappedAd == nil {
            adsList.append(AdmanBannerWrapper())
        }

        if attributeDict?["id"] != nil {
            adsList.last?.adId = attributeDict?["id"]
        }
        if attributeDict?["sequence"] != nil {
            adsList.last?.sequence = (attributeDict?["sequence"] as? NSNumber)?.intValue ?? 0
        }
    }

    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if elementName == nil {
            return
        }

        outString = (outString ?? "") + "\(string)"
    }

    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        self.elementName = nil
        let handler = NSSelectorFromString("xml\(qName ?? "")End:")
        if responds(to: handler) {
        }
        method(for: handler)
        self
        handler
        qName
        if (attributes?.count ?? 0) > 0 {
            attributes?.removeLastObject()
        }
    }

    func xmlAdSystemEnd(_ qName: String?) {
        adsList.last?.adSystem = outString
    }

    func xmlAdEnd(_ qName: String?) {
        if adsList.last?.responses != nil {
            let desc = NSSortDescriptor(key: "priority", ascending: false)
            (adsList.last?.responses as? [AnyHashable : Any])?["unknown"] = (adsList.last?.responses["unknown"] as? NSArray)?.sortedArray(using: [desc])
        }

        if let media = adsList.last?.media {
            for mediaitem in media {
                guard let mediaitem = mediaitem as? AdmanMediaFile else {
                    continue
                }
                if mediaitem.type.range(of: defaultAudioFormat).location != NSNotFound && mediaitem.bitrate == defaultBitrate {
                    adsList.last?.setSourceUrl(mediaitem.url)
                    adsList.last?.setBitrate(mediaitem.bitrate)
                }
            }
        }
        if adsList.last?.sourceUrl == nil {
            let mediaitem = getDefaultMediafile(adsList.last?.media)
            adsList.last?.setSourceUrl(mediaitem?.url)
            adsList.last?.setBitrate(mediaitem?.bitrate)
        }

        if adsList.last?.companionAds[defaultImageSize] != nil {
            adsList.last?.urlToNavigateOnClick = adsList.last?.companionAds[defaultImageSize]?.urlToNavigateOnClick
        }

        if adsList.last?.responseTime == 0 {
            adsList.last?.responseTime = -1
        }
        if adsList.last?.responseDelay == 0 {
            adsList.last?.responseDelay = -1
        }
    }

    func xmlClickTrackingEnd(_ qName: String?) {
        adsList.last?.statData.save(outString, forEvent: AdmanStatEventClickTracking)
    }

    func xmlCompanionClickThroughEnd(_ qName: String?) {
        adsList.last?.companionAds[staticResourceSize ?? ""]?.urlToNavigateOnClick = outString
    }

    func xmlCompanionClickTrackingEnd(_ qName: String?) {
        if companionTracking?["clickTracking"] == nil {
            companionTracking?["clickTracking"] = [AnyHashable](repeating: 0, count: 2)
        }
        companionTracking?["clickTracking"]?.append(outString ?? "")
    }

    func xmlClickThroughEnd(_ qName: String?) {
        adsList.last?.setUrlToNavigateOnClick(outString)
    }

    func xmlImpressionEnd(_ qName: String?) {
        adsList.last?.statData.save(outString, forEvent: AdmanStatEventImpression)
    }

    func xmlErrorEnd(_ qName: String?) {
        adsList.last?.statData.save(outString, forEvent: AdmanStatEventError)
    }

    func xmlTrackingEnd(_ qName: String?) {
        let event = attributes?.lastObject["event"] as? String

        if rootTracking {
            adsList.last?.statData.save(outString, forEvent: event)
        } else {
            companionTracking?[event ?? ""]?.append(outString ?? "")
        }
    }

    func xmlDurationEnd(_ qName: String?) {
        adsList.last?.setDuration(parseDuration(outString))
    }

    func xmlExtensionEnd(_ qName: String?) {
        let extensionType = attributes?.lastObject["type"] as? String
        if extensionType == nil {
            return
        }

        if extensionType == "ResponseTime" {
            adsList.last?.responseTime = Int(outString ?? "") ?? 0
        } else if extensionType == "ResponseMicOnSound" {
            adsList.last?.assets[AdmanBannerAssetMicOnUrl] = outString
        } else if extensionType == "ResponseMicOffSound" {
            adsList.last?.assets[AdmanBannerAssetMicOffUrl] = outString
        } else if extensionType == "ResponseDelay" {
            adsList.last?.responseDelay = Int(outString ?? "") ?? 0
        } else if extensionType == "ResponseLanguage" {
            adsList.last?.responseLang = outString
        } else if extensionType == "IntroAudio" {
            adsList.last?.setIntroUrl(outString)
        } else if extensionType == "ResponseUrl" {
            adsList.last?.responseURL = outString
        } else if extensionType == "linkTxt" {
            adsList.last?.setAdText(outString?.kv_encodeHTMLCharacterEntities())
        } else if extensionType == "AdId" {
            adsList.last?.crossServerAdID = outString
        } else if extensionType == "Expires" {
            adsList.last?.setExpirationTime(Int(outString ?? "") ?? 0)
        } else if extensionType == "controls" {
            adsList.last?.showControls = Int(outString ?? "") ?? 0
        } else if extensionType == "isClickable" {
            adsList.last?.isClickable = Int(outString ?? "") ?? 0
        }
    }

    func xmlCompanionEnd(_ qName: String?) {
        staticResourceSize = nil
        rootTracking = true
        companionTracking = nil
    }

    func xmlExpiresEnd(_ qName: String?) {
        adsList.last?.setExpirationTime(Int(outString ?? "") ?? 0)
    }

    func xmlStaticResourceEnd(_ qName: String?) {
        adsList.last?.companionAds.setValue(AdmanCreative(), forKey: staticResourceSize ?? "")
        adsList.last?.companionAds[staticResourceSize ?? ""].tracking = companionTracking

        adsList.last?.companionAds[staticResourceSize ?? ""].staticResource = outString
        if staticResourceSize == defaultImageSize {
            adsList.last?.setUrl(outString)
        }
    }

    func xmlMediaFileEnd(_ qName: String?) {
        let mediaitem = AdmanMediaFile(
            source: outString,
            type: attributes?.lastObject["type"] as? String,
            andBitrate: parseBitrate(attributes?.lastObject["bitrate"] as? String))
        if response != nil {
            response?.values.append(AdmanResponseValue(type: "media", andData: outString))
        } else {
            (adsList.last?.media as? [AnyHashable])?.append(mediaitem)
        }
    }

    func xmlVASTAdTagURIEnd(_ qName: String?) {
        adsList.last?.redirectUrl = outString
    }

    func xmlResponseEnd(_ qName: String?) {
        response = nil
    }

    func xmlValueEnd(_ qName: String?) {
        if response?.values.last?.data == nil {
            response?.values.last?.data = outString
        }
    }

    func xmlKeywordEnd(_ qName: String?) {
        response?.keywords.append(outString?.lowercased() ?? "")
    }

    func parserDidEndDocument(_ parser: XMLParser) {
        attributes = nil
        outString = nil
        elementName = nil
        response = nil
        staticResourceSize = nil
        if PARSER_DEBUG_ENABLED {
            print("VAST parser stoped")
        }
    }

    func parseBitrate(_ bitrate: String?) -> Int {
        if (bitrate?.count ?? 0) > 3 {
            return Int((bitrate as NSString?)?.substring(with: NSRange(location: 0, length: 3)) ?? "") ?? 0
        } else {
            return Int(bitrate ?? "") ?? 0
        }
    }

    func parseDuration(_ t: String?) -> Int {
        let digits = t?.components(separatedBy: ":")
        return Int(digits?[0] ?? "") ?? 0 * 3600 + Int(digits?[1] ?? "") ?? 0 * 60 + Int(digits?[2] ?? "") ?? 0
    }

    func getDefaultMediafile(_ mediafiles: [AdmanMediaFile]?) -> AdmanMediaFile? {
        for item in mediafiles ?? [] {
            if item.type.range(of: "mp3").location != NSNotFound || item.type.range(of: "mpeg").location != NSNotFound {
                return item
            }
        }
        return mediafiles?.first
    }

    class func getDefaultImageSize() -> String? {
        return defaultImageSize
    }

    class func setDefaultImageSize(_ size: String?) {
        defaultImageSize = size ?? ""
    }

    class func setDefaultAudioFormat(_ format: String?, withBitrate bitrate: NSNumber?) {
        defaultAudioFormat = format ?? ""
        defaultBitrate = (bitrate?.intValue ?? 0 > 0) ? bitrate?.intValue ?? 0 : 128
    }
}

class AdmanMediaFile {
    init(source: String?, type: String?, andBitrate bitrate: Int) {
        super.init()
        url = source
        self.type = type
        self.bitrate = bitrate
    }
}
