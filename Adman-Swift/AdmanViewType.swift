/// Copyright 2019 Instreamatic
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation
import UIKit

//if !AdmanViewType_h {
//#define AdmanViewType_h


enum AdmanViewElement : Int {
    case admanViewContainer = 1
    case admanViewBanner
    case admanViewClose
    case admanViewPlay
    case admanViewPause
    case admanViewRewind
    case admanViewRemainingTime
    case admanViewVoiceContainer
    case admanViewPositive
    case admanViewNegative
    case admanViewMicStatus
    case admanViewVoiceRequestProgress
}

class AdmanViewBase: NSObject {
    func getView() -> UIView? {
        return nil
    }
}

class AdmanViewBaseViewController: AdmanViewBase {
    private var view: UIViewController?

    init(_ view: UIViewController?) {
        super.init()
        self.view = view
    }

    override func getView() -> UIViewController? {
        return view
    }
}

class AdmanViewBaseView: AdmanViewBase {
    private weak var view: UIView?

    init(_ view: UIView?) {
        super.init()
        self.view = view
    }

    override func getView() -> UIView? {
        return view
    }
}

class AdmanViewBaseButton: AdmanViewBase {
    private weak var view: UIButton?

    init(_ view: UIButton?) {
        super.init()
        self.view = view
    }

    override func getView() -> UIButton? {
        return view
    }
}

class AdmanViewBaseLabel: AdmanViewBase {
    private weak var view: UILabel?

    init(_ view: UILabel?) {
        super.init()
        self.view = view
    }

    override func getView() -> UILabel? {
        return view
    }
}

class AdmanViewBaseImageView: AdmanViewBase {
    private weak var view: UIImageView?

    init(_ view: UIImageView?) {
        super.init()
        self.view = view
    }

    override func getView() -> UIImageView? {
        return view
    }
}

class AdmanViewBind: NSObject {
    var view: AdmanViewBase?
    var type: AdmanViewElement!

    init(_ type: AdmanViewElement, withView view: AdmanViewBase?) {
        super.init()
        self.type = type
        self.view = view
    }
}

protocol IAdmanViewBundle: NSObjectProtocol {
    func contains(_ type: AdmanViewElement) -> Bool
    func get(_ type: AdmanViewElement) -> AdmanViewBase?
}

/*
@interface AdmanViewBundle: NSObject<IAdmanViewBundle>
- (_Null_unspecified id)init:(NSMutableArray<AdmanViewBind *> * _Nonnull)bindings;
@end
*/

//}
//AdmanViewBase//AdmanViewBaseViewController//AdmanViewBaseView//AdmanViewBaseButton//AdmanViewBaseLabel//AdmanViewBaseImageView//AdmanViewBind
//AdmanViewBundle
/*
@interface AdmanViewBundle()

@property (strong) NSMutableDictionary*  byId;

@end

@implementation AdmanViewBundle

- (_Null_unspecified id)init:(NSMutableArray<AdmanViewBind *> * _Nonnull)bindings{
    self = [super init];
    if (self) {
        _byId = [NSMutableDictionary dictionary];
        for (AdmanViewBind *item in bindings) {
            [_byId setObject:item forKey:item.type];
        }
    }
    return self;
}

- (bool) contains:(AdmanViewType) type{
    return ([[_byId allKeys] containsObject:type]);
}

- (nullable AdmanViewBase*) get:(AdmanViewType) type{
    return [_byId objectForKey:  type];
}

@end

*/
