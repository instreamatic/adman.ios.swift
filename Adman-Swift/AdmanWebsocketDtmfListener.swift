/// Copyright 2018 Instreamatic
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation
//import SocketRocket

private var AdmanDtmfWebsocketDomain = "wss://dtmf.instreamatic.com:8081/dtmfws/sub"

protocol AdmanDtmfDelegate: NSObjectProtocol {
    func dtmfIn()
    func dtmfOut()
}

class AdmanWebsocketDtmfListener: NSObject, SRWebSocketDelegate {
    var delegate: Any?
    var connection: SRWebSocket?

    func start(_ station_key: String?) {
        let domain = "\(AdmanDtmfWebsocketDomain)/\(station_key ?? "")"
        print("Open connection over websocket \(domain)")
        var r: NSMutableURLRequest? = nil
        if let url = URL(string: domain) {
            r = NSMutableURLRequest(url: url)
        }
        connection = SRWebSocket(urlRequest: r)
        connection?.delegate = self
        connection?.open()

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(5 * Double(NSEC_PER_SEC)) / Double(NSEC_PER_SEC), execute: { [self] in
            if connection != nil && connection?.readyState != SR_OPEN {
                print("Failed to open websocket")
                connection?.close()
                connection = nil
                start(station_key)
            }
        })

        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(pingConnection), userInfo: nil, repeats: true)
    }

    @objc func pingConnection() {
        if connection == nil {
            return
        }
        if connection?.readyState == SR_OPEN {
            connection?.sendPing(nil)
        }
    }

    func stop() {
        connection?.close()
        connection = nil
    }

    func webSocketDidOpen(_ webSocket: SRWebSocket?) {
        print("libAdman: dtmf listener opened")
    }

    func webSocket(_ webSocket: SRWebSocket?, didReceiveMessage message: Any?) {
        var jsonError: Error?
        if let message = message {
            print("libAdman: dtmf recived: \(message)")
        }
        let data = message?.data(using: .utf8)
        var json: [AnyHashable : Any]? = nil
        do {
            if let data = data {
                json = try JSONSerialization.jsonObject(
                    with: data,
                    options: .mutableContainers) as? [AnyHashable : Any]
            }
        } catch let jsonError {
        }
        if json?["label"] == "IN" {
            delegate?.dtmfIn()
        } else if json?["label"] == "OUT" {
            delegate?.dtmfOut()
        }
    }

    func webSocket(_ webSocket: SRWebSocket?, didFailWithError error: Error?) {
        print("libAdman: Dtmf listener recive error, \(error?.localizedDescription ?? "")")
    }

    func webSocket(_ webSocket: SRWebSocket?, didCloseWithCode code: Int, reason: String?, wasClean: Bool) {
        print("libAdman: dtmf listener closed")
    }

    func webSocket(_ webSocket: SRWebSocket?, didReceivePong pongPayload: Data?) {
        // NSLog(@"libAdman: dtmf listener, pong recived");
    }
}
