//  NSString+HTML.swift
//  Koolistov
//
//  Created by Johan Kool on 13-11-09.
//  Copyright 2009-2011 Koolistov Pte. Ltd. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are 
//  permitted provided that the following conditions are met:
//
//  * Redistributions of source code must retain the above copyright notice, this list of 
//    conditions and the following disclaimer.
//  * Neither the name of KOOLISTOV PTE. LTD. nor the names of its contributors may be used to 
//    endorse or promote products derived from this software without specific prior written 
//    permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
//  THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
//  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
//  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
//  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


//
//  NSString+Koolistov.m
//  Koolistov
//
//  Created by Johan Kool on 13-11-09.
//  Copyright 2009-2011 Koolistov Pte. Ltd. All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are 
//  permitted provided that the following conditions are met:
//
//  * Redistributions of source code must retain the above copyright notice, this list of 
//    conditions and the following disclaimer.
//  * Neither the name of KOOLISTOV PTE. LTD. nor the names of its contributors may be used to 
//    endorse or promote products derived from this software without specific prior written 
//    permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
//  THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
//  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
//  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
//  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//


import Foundation

extension String {

    func kv_decodeHTMLCharacterEntities() -> String? {
        if (self as NSString).range(of: "&").location == NSNotFound {
            return self as String
        } else {
            var escaped = self as String
            let codes = ["&nbsp;", "&iexcl;", "&cent;", "&pound;", "&curren;", "&yen;", "&brvbar;", "&sect;", "&uml;", "&copy;", "&ordf;", "&laquo;", "&not;", "&shy;", "&reg;", "&macr;", "&deg;", "&plusmn;", "&sup2;", "&sup3;", "&acute;", "&micro;", "&para;", "&middot;", "&cedil;", "&sup1;", "&ordm;", "&raquo;", "&frac14;", "&frac12;", "&frac34;", "&iquest;", "&Agrave;", "&Aacute;", "&Acirc;", "&Atilde;", "&Auml;", "&Aring;", "&AElig;", "&Ccedil;", "&Egrave;", "&Eacute;", "&Ecirc;", "&Euml;", "&Igrave;", "&Iacute;", "&Icirc;", "&Iuml;", "&ETH;", "&Ntilde;", "&Ograve;", "&Oacute;", "&Ocirc;", "&Otilde;", "&Ouml;", "&times;", "&Oslash;", "&Ugrave;", "&Uacute;", "&Ucirc;", "&Uuml;", "&Yacute;", "&THORN;", "&szlig;", "&agrave;", "&aacute;", "&acirc;", "&atilde;", "&auml;", "&aring;", "&aelig;", "&ccedil;", "&egrave;", "&eacute;", "&ecirc;", "&euml;", "&igrave;", "&iacute;", "&icirc;", "&iuml;", "&eth;", "&ntilde;", "&ograve;", "&oacute;", "&ocirc;", "&otilde;", "&ouml;", "&divide;", "&oslash;", "&ugrave;", "&uacute;", "&ucirc;", "&uuml;", "&yacute;", "&thorn;", "&yuml;"]

            var i: Int
            let count = codes.count

            // MARK: Html
            for i in 0..<count {
                var range = (self as NSString).range(of: codes[i])
                if range.location != NSNotFound {
                    if let subRange = Range<String.Index>(NSRange(location: 0, length: escaped.count), in: escaped) { escaped = escaped.replacingOccurrences(of: codes[i], with: "\(UInt16(160 + i))", options: .literal, range: subRange) }
                }
            }

            // The following five are not in the 160+ range

            // MARK: @"&amp;"
            var range = (self as NSString).range(of: "&amp;")
            if range.location != NSNotFound {
                if let subRange = Range<String.Index>(NSRange(location: 0, length: escaped.count), in: escaped) { escaped = escaped.replacingOccurrences(of: "&amp;", with: "\(UInt16(38))", options: .literal, range: subRange) }
            }

            // MARK: @"&lt;"
            range = (self as NSString).range(of: "&lt;")
            if range.location != NSNotFound {
                if let subRange = Range<String.Index>(NSRange(location: 0, length: escaped.count), in: escaped) { escaped = escaped.replacingOccurrences(of: "&lt;", with: "\(UInt16(60))", options: .literal, range: subRange) }
            }

            // MARK: @"&gt;"
            range = (self as NSString).range(of: "&gt;")
            if range.location != NSNotFound {
                if let subRange = Range<String.Index>(NSRange(location: 0, length: escaped.count), in: escaped) { escaped = escaped.replacingOccurrences(of: "&gt;", with: "\(UInt16(62))", options: .literal, range: subRange) }
            }

            // MARK: @"&apos;"
            range = (self as NSString).range(of: "&apos;")
            if range.location != NSNotFound {
                if let subRange = Range<String.Index>(NSRange(location: 0, length: escaped.count), in: escaped) { escaped = escaped.replacingOccurrences(of: "&apos;", with: "\(UInt16(39))", options: .literal, range: subRange) }
            }

            // MARK: @"&quot;"
            range = (self as NSString).range(of: "&quot;")
            if range.location != NSNotFound {
                if let subRange = Range<String.Index>(NSRange(location: 0, length: escaped.count), in: escaped) { escaped = escaped.replacingOccurrences(of: "&quot;", with: "\(UInt16(34))", options: .literal, range: subRange) }
            }

            // MARK: Decimal & Hex
            var start: NSRange
            var finish: NSRange
            var searchRange = NSRange(location: 0, length: escaped.count)
            i = 0

            while i < escaped.count {
                start = (escaped as NSString).range(of: "&#", options: .caseInsensitive, range: searchRange)

                finish = (escaped as NSString).range(of: ";", options: .caseInsensitive, range: searchRange)

                if start.location != NSNotFound && finish.location != NSNotFound && finish.location > start.location {
                    let entityRange = NSRange(location: start.location, length: (finish.location - start.location) + 1)
                    let entity = (escaped as NSString).substring(with: entityRange)
                    let value = (entity as NSString).substring(with: NSRange(location: 2, length: entity.count - 2))

                    if let subRange = Range<String.Index>(entityRange, in: escaped) { escaped.removeSubrange(subRange) }

                    if value.hasPrefix("x") {
                        var tempInt: UInt = 0
                        let scanner = Scanner(string: (value as NSString).substring(from: 1))
                        scanner.scanHexInt32(UnsafeMutablePointer<UInt32>(mutating: &tempInt))
                        escaped.insert(contentsOf: "\(UInt16(tempInt))", at: s.index(s.startIndex, offsetBy: entityRange.location))
                    } else {
                        escaped.insert(contentsOf: "\(UInt16(Int(value) ?? 0))", at: s.index(s.startIndex, offsetBy: entityRange.location))
                    }
                    i = start.location
                } else {
                    i += 1
                }
                searchRange = NSRange(location: i, length: escaped.count - i)
            }

            return escaped // Note this is autoreleased
        }
    }

    func kv_encodeHTMLCharacterEntities() -> String? {
        var encoded = self as String

        // MARK: @"&amp;"
        var range = (self as NSString).range(of: "&")
        if range.location != NSNotFound {
            if let subRange = Range<String.Index>(NSRange(location: 0, length: encoded.count), in: encoded) { encoded = encoded.replacingOccurrences(of: "&", with: "&amp;", options: .literal, range: subRange) }
        }

        // MARK: @"&lt;"
        range = (self as NSString).range(of: "<")
        if range.location != NSNotFound {
            if let subRange = Range<String.Index>(NSRange(location: 0, length: encoded.count), in: encoded) { encoded = encoded.replacingOccurrences(of: "<", with: "&lt;", options: .literal, range: subRange) }
        }

        // MARK: @"&gt;"
        range = (self as NSString).range(of: ">")
        if range.location != NSNotFound {
            if let subRange = Range<String.Index>(NSRange(location: 0, length: encoded.count), in: encoded) { encoded = encoded.replacingOccurrences(of: ">", with: "&gt;", options: .literal, range: subRange) }
        }

        return encoded
    }
}
