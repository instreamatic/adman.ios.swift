/*
 Copyright (c) 2011, Tony Million.
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.

 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 */

// import arpa
import Foundation
// import netinet
// import netinet6
// import sys
import SystemConfiguration

//! Project version number for MacOSReachability.
var ReachabilityVersionNumber = 0.0
//! Project version string for MacOSReachability.
let ReachabilityVersionString = [UInt8](repeating: 0, count: 10)
/// Create NS_ENUM macro if it does not exist on the targeted version of iOS or OS X.
///
/// - seealso: http://nshipster.com/ns_enum-ns_options/
///
// if !NS_ENUM {
//#define NS_ENUM(_type, _name) enum _name : _type _name; enum _name : _type
// }
enum NetworkState : Int {
    // Apple NetworkStatus Compatible Names.
    case notReachable = 0
    case reachableViaWiFi = 2
    case reachableViaWWAN = 1
}

typealias NetworkReachable = (NetworkStatus?) -> Void
typealias NetworkUnreachable = (NetworkStatus?) -> Void
typealias NetworkReachability = (NetworkStatus?, SCNetworkConnectionFlags) -> Void
let kReachabilityChangedNotification = "kReachabilityChangedNotification"

class NetworkStatus: NSObject {
    var reachableBlock: NetworkReachable?
    var unreachableBlock: NetworkUnreachable?
    var reachabilityBlock: NetworkReachability?
    var reachableOnWWAN = false
    private var reachabilityRef: SCNetworkReachability?
    private var reachabilitySerialQueue: DispatchQueue?
    private var reachabilityObject: Any?

    // This is identical to the function above, but is here to maintain
    //compatibility with Apples original code. (see .m)

    // MARK: - Class Constructor Methods

    class func reachability(withHostName hostname: String?) -> Self {
        return NetworkStatus.reachability(withHostname: hostname)
    }

    class func reachability(withHostname hostname: String?) -> Self {
        var ref: SCNetworkReachability? = nil
        if let utf = hostname?.utf8CString {
            ref = SCNetworkReachabilityCreateWithName(nil, utf)
        }
        if let ref = ref {
            let reachability = self.init(reachabilityRef: ref)

            return reachability
        }

        return nil
    }

    class func reachability(withAddress hostAddress: UnsafeMutableRawPointer?) -> Self {
        var ref: SCNetworkReachability? = nil
        if let address = hostAddress as? sockaddr {
            ref = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, address)
        }
        if let ref = ref {
            let reachability = self.init(reachabilityRef: ref)

            return reachability
        }

        return nil
    }

    class func reachabilityForInternetConnection() -> Self {
        var zeroAddress: sockaddr_in
        bzero(&zeroAddress, MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_len = MemoryLayout.size(ofValue: zeroAddress)
        zeroAddress.sin_family = AF_INET

        return self.reachability(withAddress: &zeroAddress)
    }

    class func reachabilityForLocalWiFi() -> Self {
        var localWifiAddress: sockaddr_in
        bzero(&localWifiAddress, MemoryLayout.size(ofValue: localWifiAddress))
        localWifiAddress.sin_len = MemoryLayout.size(ofValue: localWifiAddress)
        localWifiAddress.sin_family = AF_INET
        // IN_LINKLOCALNETNUM is defined in <netinet/in.h> as 169.254.0.0
        localWifiAddress.sin_addr.s_addr = htonl(IN_LINKLOCALNETNUM)

        return self.reachability(withAddress: &localWifiAddress)
    }

    class func reachability(with url: URL?) -> Self {
        var reachability: Any?

        let host = url?.host
        let isIpAddress = self.isIpAddress(host)

        if isIpAddress {
            let port = url?.port ?? (url?.scheme == "https") ? NSNumber(value: 443) : NSNumber(value: 80)

            var address: sockaddr_in
            address.sin_len = MemoryLayout.size(ofValue: address)
            address.sin_family = AF_INET
            address.sin_port = htons(port.intValue)
            address.sin_addr.s_addr = inet_addr(host?.utf8CString)

            reachability = self.reachability(withAddress: &address)
        } else {
            reachability = self.reachability(withHostname: host)
        }

        return reachability as! NetworkStatus
    }

    class func isIpAddress(_ host: String?) -> Bool {
        var pin: in_addr
        return 1 == inet_aton(host?.utf8CString, &pin)
    }

    // Initialization methods

    init(reachabilityRef ref: SCNetworkReachability?) {
        super.init()
        reachableOnWWAN = true
        reachabilityRef = ref

        // We need to create a serial queue.
        // We allocate this once for the lifetime of the notifier.

        reachabilitySerialQueue = DispatchQueue(label: "com.tonymillion.reachability")
    }

    deinit {
        stopNotifier()

        if let reachabilityRef = reachabilityRef {
            reachabilityRef = nil
        }

        reachableBlock = nil
        unreachableBlock = nil
        reachabilityBlock = nil
        reachabilitySerialQueue = nil
    }

    // MARK: - Notifier Methods

    // Notifier
    // NOTE: This uses GCD to trigger the blocks - they *WILL NOT* be called on THE MAIN THREAD
    // - In other words DO NOT DO ANY UI UPDATES IN THE BLOCKS.
    //   INSTEAD USE dispatch_async(dispatch_get_main_queue(), ^{UISTUFF}) (or dispatch_sync if you want)

    func startNotifier() -> Bool {
        // allow start notifier to be called multiple times
        if reachabilityObject != nil && ((reachabilityObject as? NetworkStatus) == self) {
            return true
        }


        var context = SCNetworkReachabilityContext(version: CFIndex(0), info: nil, retain: nil, release: nil, copyDescription: nil)
        context.info = self

        if let reachabilityRef = reachabilityRef {
            if SCNetworkReachabilitySetCallback(reachabilityRef, TMReachabilityCallback, &context) {
                // Set it as our reachability queue, which will retain the queue
                if SCNetworkReachabilitySetDispatchQueue(reachabilityRef, reachabilitySerialQueue) {
                    // this should do a retain on ourself, so as long as we're in notifier mode we shouldn't disappear out from under ourselves
                    // woah
                    reachabilityObject = self
                    return true
                } else {
                    if DEBUG {
                    print("SCNetworkReachabilitySetDispatchQueue() failed: \(SCErrorString(SCError()))")
                    }

                    // UH OH - FAILURE - stop any callbacks!
                    SCNetworkReachabilitySetCallback(reachabilityRef, nil, nil)
                }
            } else {
                if DEBUG {
                print("SCNetworkReachabilitySetCallback() failed: \(SCErrorString(SCError()))")
                }
            }
        }

        // if we get here we fail at the internet
        reachabilityObject = nil
        return false
    }

    func stopNotifier() {
        // First stop, any callbacks!
        if let reachabilityRef = reachabilityRef {
            SCNetworkReachabilitySetCallback(reachabilityRef, nil, nil)
        }

        // Unregister target from the GCD serial dispatch queue.
        if let reachabilityRef = reachabilityRef {
            SCNetworkReachabilitySetDispatchQueue(reachabilityRef, nil)
        }

        reachabilityObject = nil
    }

    // MARK: - reachability tests

    // This is for the case where you flick the airplane mode;
    // you end up getting something like this:
    //Reachability: WR ct-----
    //Reachability: -- -------
    //Reachability: WR ct-----
    //Reachability: -- -------
    // We treat this as 4 UNREACHABLE triggers - really apple should do better than this

    let testcase = [.connectionRequired, .transientConnection]

    func isReachable(with flags: SCNetworkReachabilityFlags) -> Bool {
        var connectionUP = true

        if (flags.rawValue & SCNetworkReachabilityFlags.reachable.rawValue) == 0 {
            connectionUP = false
        }

        if (flags.rawValue & testcase.rawValue) == testcase.rawValue {
            connectionUP = false
        }

        #if os(iOS)
        if flags.rawValue & SCNetworkReachabilityFlags.isWWAN.rawValue != 0 {
            // We're on 3G.
            if !reachableOnWWAN {
                // We don't want to connect when on 3G.
                connectionUP = false
            }
        }
        #endif

        return connectionUP
    }

    func isReachable() -> Bool {
        var flags: SCNetworkReachabilityFlags

        if let reachabilityRef = reachabilityRef {
            if !SCNetworkReachabilityGetFlags(reachabilityRef, &flags) {
                return false
            }
        }

        return isReachable(with: flags)
    }

    func isReachableViaWWAN() -> Bool {
        #if os(iOS)

        var flags: SCNetworkReachabilityFlags = []

        if let reachabilityRef = reachabilityRef {
            if SCNetworkReachabilityGetFlags(reachabilityRef, &flags) {
                // Check we're REACHABLE
                if flags.rawValue & SCNetworkReachabilityFlags.reachable.rawValue != 0 {
                    // Now, check we're on WWAN
                    if flags.rawValue & SCNetworkReachabilityFlags.isWWAN.rawValue != 0 {
                        return true
                    }
                }
            }
        }
        #endif

        return false
    }

    func isReachableViaWiFi() -> Bool {
        var flags: SCNetworkReachabilityFlags = []

        if let reachabilityRef = reachabilityRef {
            if SCNetworkReachabilityGetFlags(reachabilityRef, &flags) {
                // Check we're reachable
                if (flags.rawValue & SCNetworkReachabilityFlags.reachable.rawValue) != 0 {
                    #if os(iOS)
                    // Check we're NOT on WWAN
                    if (flags.rawValue & SCNetworkReachabilityFlags.isWWAN.rawValue) != 0 {
                        return false
                    }
                    #endif
                    return true
                }
            }
        }

        return false
    }

    // WWAN may be available, but not active until a connection has been established.
    // WiFi may require a connection for VPN on Demand. // Identical DDG variant.
    func isConnectionRequired() -> Bool {
        return connectionRequired()
    }

 // Apple's routine.
    func connectionRequired() -> Bool {
        var flags: SCNetworkReachabilityFlags

        if let reachabilityRef = reachabilityRef {
            if SCNetworkReachabilityGetFlags(reachabilityRef, &flags) {
                return (flags.rawValue & SCNetworkReachabilityFlags.connectionRequired.rawValue)
            }
        }

        return false
    }

    // Dynamic, on demand connection?
    func isConnectionOnDemand() -> Bool {
        var flags: SCNetworkReachabilityFlags

        if let reachabilityRef = reachabilityRef {
            if SCNetworkReachabilityGetFlags(reachabilityRef, &flags) {
                return (flags.rawValue & SCNetworkReachabilityFlags.connectionRequired.rawValue) != 0 && (flags.rawValue & ([.connectionOnTraffic, .connectionOnDemand]).rawValue) != 0
            }
        }

        return false
    }

    // Is user intervention required?
    func isInterventionRequired() -> Bool {
        var flags: SCNetworkReachabilityFlags

        if let reachabilityRef = reachabilityRef {
            if SCNetworkReachabilityGetFlags(reachabilityRef, &flags) {
                return (flags.rawValue & SCNetworkReachabilityFlags.connectionRequired.rawValue) != 0 && (flags.rawValue & SCNetworkReachabilityFlags.interventionRequired.rawValue) != 0
            }
        }

        return false
    }

    // MARK: - reachability status stuff

    func currentReachabilityStatus() -> NetworkState {
        if isReachable() {
            if isReachableViaWiFi() {
                return .reachableViaWiFi
            }

            #if os(iOS)
            return .reachableViaWWAN
            #endif
        }

        return .notReachable
    }

    func reachabilityFlags() -> SCNetworkReachabilityFlags {
        var flags: SCNetworkReachabilityFlags = []

        if let reachabilityRef = reachabilityRef {
            if SCNetworkReachabilityGetFlags(reachabilityRef, &flags) {
                return flags
            }
        }

        return []
    }

    func currentReachabilityString() -> String? {
        let temp = currentReachabilityStatus()

        if temp == .reachableViaWWAN {
            // Updated for the fact that we have CDMA phones now!
            return NSLocalizedString("Cellular", comment: "")
        }
        if temp == .reachableViaWiFi {
            return NSLocalizedString("WiFi", comment: "")
        }

        return NSLocalizedString("No Connection", comment: "")
    }

    func currentReachabilityFlags() -> String? {
        return reachabilityFlags(reachabilityFlags())
    }

    // MARK: - Callback function calls this method

    func reachabilityChanged(_ flags: SCNetworkReachabilityFlags) {
        if isReachable(with: flags) {
            if reachableBlock != nil {
                reachableBlock?(self)
            }
        } else {
            if unreachableBlock != nil {
                unreachableBlock?(self)
            }
        }

        if reachabilityBlock != nil {
            reachabilityBlock?(self, SCNetworkConnectionFlags(flags))
        }

        // this makes sure the change notification happens on the MAIN THREAD
        DispatchQueue.main.async(execute: {
            NotificationCenter.default.post(
                name: NSNotification.Name(kReachabilityChangedNotification),
                object: self)
        })
    }

    // MARK: - Debug Description

    override var description: String {
        let description = String(format: "<%@: %p (%@)>", NSStringFromClass(NetworkStatus.self), self, currentReachabilityFlags() ?? "")
        return description
    }
}

private func reachabilityFlags(_ flags: SCNetworkReachabilityFlags) -> String? {
    return "\((flags.rawValue & SCNetworkReachabilityFlags.isWWAN.rawValue) != 0 ? "W" : "-")\("X") \((flags.rawValue & SCNetworkReachabilityFlags.reachable.rawValue) != 0 ? "R" : "-")\((flags.rawValue & SCNetworkReachabilityFlags.connectionRequired.rawValue) != 0 ? "c" : "-")\((flags.rawValue & SCNetworkReachabilityFlags.transientConnection.rawValue) != 0 ? "t" : "-")\((flags.rawValue & SCNetworkReachabilityFlags.interventionRequired.rawValue) != 0 ? "i" : "-")\((flags.rawValue & SCNetworkReachabilityFlags.connectionOnTraffic.rawValue) != 0 ? "C" : "-")\((flags.rawValue & SCNetworkReachabilityFlags.connectionOnDemand.rawValue) != 0 ? "D" : "-")\((flags.rawValue & SCNetworkReachabilityFlags.isLocalAddress.rawValue) != 0 ? "l" : "-")"
}

// Start listening for reachability notifications on the current run loop
private func TMReachabilityCallback(_ target: SCNetworkReachability?, _ flags: SCNetworkReachabilityFlags, _ info: UnsafeMutableRawPointer?) {
    //#pragma unused (target)

    let reachability = info as? NetworkStatus

    // We probably don't need an autoreleasepool here, as GCD docs state each queue has its own autorelease pool,
    // but what the heck eh?
    autoreleasepool {
        reachability?.reachabilityChanged(flags)
    }
}
