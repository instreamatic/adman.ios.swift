/// Copyright 2018 Instreamatic
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation
import SocketRocket

typealias VACConnectionInit = () -> Void
enum ServerVoiceMessageType : Int {
    case voiceMessageAudioStart
    case voiceMessageTranscript
    case voiceMessageResult
}

private var AdmanRecognitionUrl: String? = nil

protocol VACNetworkManagerDelegate: NSObjectProtocol {
    func networkManagerConnectionDidOpened(_ manager: VACNetworkManager)
    func networkManager(_ manager: VACNetworkManager, didResponseRecived response: [AnyHashable : Any]?)
    func networkManager(_ manager: VACNetworkManager, didFailWithError error: Error?)
    func networkManager(_ manager: VACNetworkManager, didSessionEnded response: [AnyHashable : Any]?)
    func networkManager(_ manager: VACNetworkManager, didCommandRecived command: String?)
}

class VACNetworkManager: NSObject, SRWebSocketDelegate {
    weak var delegate: VACNetworkManagerDelegate?
    private var headerSended = false
    private var connectionOptions: [AnyHashable : Any]?
    private var connection: SRWebSocket?
    private var initHandler: VACConnectionInit?

    class func setWebsocketUrl(_ url: String?) {
        AdmanRecognitionUrl = url
    }

    class func getWebSocketUrl() -> String? {
        return AdmanRecognitionUrl
    }

    func openConnection(_ handler: VACConnectionInit, withOptions options: [AnyHashable : Any]?) {
        print("voice server url: \(AdmanRecognitionUrl ?? "")")

        var request: NSMutableURLRequest? = nil
        if let url = URL(string: AdmanRecognitionUrl ?? "") {
            request = NSMutableURLRequest(url: url)
        }
        if #available(iOS 12.0, *) {
            request?.networkServiceType = .background
        }

        connection = SRWebSocket(urlRequest: request)
        initHandler = handler
        headerSended = false
        connectionOptions = options

        connection?.delegate = self
        connection?.open()
    }

    func toMessage(_ msg: String, withSamples data: Data?) -> Data? {
        var request = Data()
        let mSize = msg.data(using: .utf8)?.count ?? 0

        let bSize = [(mSize >> 8) & 0xff, mSize & 0xff]
        request.append(&bSize, length: 2)
        if let data1 = msg.data(using: .utf8) {
            request.append(data1)
        }
        if let data = data {
            request.append(data)
        }
        return request
    }

    func closeConnection() {
        if connection == nil {
            return
        }

        connection?.close()
        initHandler = nil
        connection = nil
        headerSended = false

    }

    func isOpen() -> Bool {
        return connection != nil && connection?.readyState == SR_OPEN
    }

    func send(_ data: Data?) {
        if connection?.readyState == SR_OPEN {
            connection?.send(data)
        }
    }

    func send(_ data: Data?) {
        if connection?.readyState == SR_OPEN && headerSended {
            connection?.send(toMessage("content-type: audio/wav\r\npath: audio.data\r\n", withSamples: data))
        }
    }

    // MARK: webSocket delegate

    func webSocketDidOpen(_ webSocket: SRWebSocket?) {
        if connection?.readyState != SR_OPEN {
            return
        }

        if initHandler != nil {
            initHandler?()
            initHandler = nil
        }

        var request_data: String? = nil
        if let _connectionOption = connectionOptions?["ad_id"], let _connectionOption1 = connectionOptions?["response_delay"], let _connectionOption2 = connectionOptions?["device_info"], let _connectionOption3 = connectionOptions?["idfa"] {
            request_data = String(
                format: "{\"partner_id\": 1, \"site_id\": %i, \"ad_id\": \"%@\", \"response_delay\": %@, \"device_info\": \"%@\", \"idfa\": \"%@\", \"vad\": %i}", (connectionOptions?["site_id"] as? NSNumber)?.intValue ?? 0, connectionOptions?["ad_id"], connectionOptions?["response_delay"], connectionOptions?["device_info"], connectionOptions?["idfa"], (connectionOptions?["vad"] as? NSNumber)?.intValue ?? 0)
        }
        connection?.send(toMessage("content-type: application/json\r\npath: request\r\n\r\n", withSamples: request_data?.data(using: .utf8)))
        // NSLog(@"Adman.wss: headers sended");
    }

    func webSocket(_ webSocket: SRWebSocket?, didReceiveMessage message: Any?) {

        let result = message?.components(separatedBy: "\r\n\r\n")
        var headers: [AnyHashable : Any] = [:]

        for header in result?[0]?.components(separatedBy: "\r\n") ?? [] {
            let list = header.components(separatedBy: ":")
            headers[list[0].lowercased()] = list[1].trimmingCharacters(in: CharacterSet.whitespaces)
        }

        // start message
        if (headers["path"] == "audio.start") && connection?.readyState == SR_OPEN {
            print("Send wav header to server")
            connection?.send(toMessage("content-type: audio/wav\r\npath: audio.data\r\n", withSamples: VACRecorder.wavHeader()))
            headerSended = true
            delegate?.networkManagerConnectionDidOpened(self)
            return
        }

        //
        if headers["path"] == "audio.stop" {
            delegate?.networkManager(self, didCommandRecived: headers["path"] as? String)
            return
        }

        if (headers["content-type"] == "application/json") || (headers["content-type"] == "text/json") {
            var obj: [AnyHashable : Any]? = nil
            do {
                if let data = result?[1]?.data(using: .utf8) {
                    obj = try JSONSerialization.jsonObject(with: data, options: []) as? [AnyHashable : Any]
                }
            } catch {
            }
            // NSLog(@"Websocket %@ resp: %@", webSocket, message);

            // VoiceTranscript message, ignore silence
            // obj[@"transcript"] can be nil in iOS < 12
            if (headers["path"] == "voice.transcript") && obj?["transcript"] != nil {
                // obj[@"transcript"] is NSNul in iOS 12 >
                if (obj?["transcript"] != NSNull()) && (obj?["transcript"]?.length() ?? 0) > 0 {
                    delegate?.networkManager(self, didResponseRecived: obj)
                }
            }

            // Voice result message
            if headers["path"] == "voice.result" {
                // NSLog(@"Websocket, end result: %@", obj);
                delegate?.networkManager(self, didSessionEnded: obj)
                closeConnection()
            }

            // error message
            if headers["path"] == "error" {
                delegate?.networkManager(
                    self,
                    didFailWithError: NSError(domain: NSNetServicesErrorDomain, code: Int(NSURLErrorCannotDecodeRawData), userInfo: obj as? [String : Any]))
                closeConnection()
            }
        }
    }

    func webSocket(_ webSocket: SRWebSocket?, didFailWithError error: Error?) {
        delegate?.networkManager(self, didFailWithError: error)
    }

    func webSocket(_ webSocket: SRWebSocket?, didCloseWithCode code: Int, reason: String?, wasClean: Bool) {
        print("Connection closed with reason \(reason ?? "")")
    }
}
