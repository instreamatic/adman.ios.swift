/// Copyright 2018 Instreamatic
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.

enum VACRecognizerState : Int {
    case none
    case ready
    case listening
}

protocol VACRecognizerDelegate: NSObjectProtocol {
    func recognizer(_ recogizer: VACRecognizer?, didChange state: VACRecognizerState)
    func recognizer(_ recogizer: VACRecognizer?, didRecognizeCommand command: [AnyHashable : Any]?)
    func recognizer(_ recogizer: VACRecognizer?, didSessionEnded result: [AnyHashable : Any]?)
    func recognizer(_ recogizer: VACRecognizer?, didFailWithError error: Error?)
    func recognizer(_ recogizer: VACRecognizer?, didConnectionOpened state: VACRecognizerState)
}

class VACRecognizer: NSObject, VACRecorderDelegate, VACNetworkManagerDelegate {
    private var samplescounter = 0
    private var docsDir: String?
    private var dirPaths: [AnyHashable]?

    private(set) var state: VACRecognizerState!
    weak var delegate: VACRecognizerDelegate?
    private var samplesPartialBuffer: [AnyHashable]?
    private var recorder: VACRecorder?
    private var networkManager: VACNetworkManager?

    override init() {
        super.init()

        state = .none
        recorder = VACRecorder()
        recorder?.delegate = self
        samplesPartialBuffer = [AnyHashable](repeating: 0, count: 1000)
        networkManager = VACNetworkManager()
        networkManager?.delegate = self

        dirPaths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).map(\.path)
        docsDir = dirPaths?[0] as? String
    }

    func setState(_ state: VACRecognizerState) {
        if _state != state {
            _state = state
            delegate?.recognizer(self, didChange: state)
        }
    }

    func start(_ onStart: VACConnectionInit, withOptions options: [AnyHashable : Any]?) {
        samplescounter = 0

        state = .ready
        print("libAdman: network recognizer started")
        networkManager?.openConnection({ [self] in
            if onStart != nil {
                onStart()
            }
            state = .listening
        }, withOptions: options)
    }

    func stop() {
        if state == .none {
            return
        }

        state = .none
        recorder?.stopRecording()
        endSpeechDetection()
        samplesPartialBuffer?.removeAll()
        print("libAdman: network recognizer stopped")
    }

    func closeConnection() {
        networkManager?.closeConnection()
    }

    func openSocket(_ socketDidOpen: VACConnectionInit) {

    }

    func startListening() {
        print("libAdman: voice recording started")

        samplesPartialBuffer?.removeAll()
        if !recorder?.isRunning {
            recorder?.startRecording()
        }
        state = .listening
    }

    func stopListening() {
        print("libAdman: voice recording stopped")

        if recorder?.isRunning {
            recorder?.stopRecording()
        }
        state = .none
    }

    func endSpeechDetection() {
        networkManager?.send(networkManager?.toMessage("content-type: audio/wav\r\npath: audio.end\r\n", withSamples: nil))
    }

    // MARK: - VACRecorderDelegate

    func processSample(
        withCapacity audioDataBytesCapacity: UInt32,
        audioData: UnsafeMutableRawPointer?
    ) {
        let sampleData = Data(bytes: audioData, length: Int(audioDataBytesCapacity))
        // NSLog(sampleData.description);

        if !networkManager?.isOpen() || (samplesPartialBuffer?.count ?? 0) > 0 {
            samplesPartialBuffer?.append(sampleData)
        } else {
            networkManager?.send(sampleData)
        }

        // NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%ld.raw", samplescounter++]]];
        // [sampleData writeToFile:databasePath atomically:YES];
    }

    // MARK: - VACNetworkManagerDelegate

    func networkManager(_ manager: VACNetworkManager?, didCommandRecived command: String?) {
        // NSLog(@"networkManagerDidCommandRecived %@", command);
        if command == "audio.stop" {
            stopListening()
        }
    }

    func networkManagerConnectionDidOpened(_ manager: VACNetworkManager?) {
        if (samplesPartialBuffer?.count ?? 0) > 0 {
            while (samplesPartialBuffer?.count ?? 0) > 0 {
                let sample = samplesPartialBuffer?[0]
                samplesPartialBuffer?.remove(at: 0)
                networkManager?.sendData(sample)
            }
        }

        print("Conection opened: \(VACNetworkManager.getWebSocketUrl())")
        delegate?.recognizer(self, didConnectionOpened: state)
    }

    func networkManager(_ manager: VACNetworkManager?, didFailWithError error: Error?) {
        if let error = error {
            print("libAdman.webSocket: didFailWithError \(error)")
        }
        // ignore Nuance silence recognition error
        if (error as NSError?)?.code == 1015 {
            delegate?.recognizer(self, didRecognizeCommand: nil)
        }

        if state != .none {
            stop()
            delegate?.recognizer(self, didFailWithError: error)
        }
    }

    func networkManager(_ manager: VACNetworkManager?, didResponseRecived response: [AnyHashable : Any]?) {
        delegate?.recognizer(self, didRecognizeCommand: response)
    }

    func networkManager(_ manager: VACNetworkManager?, didSessionEnded response: [AnyHashable : Any]?) {
        delegate?.recognizer(self, didSessionEnded: response)
        samplesPartialBuffer?.removeAll()
    }
}
