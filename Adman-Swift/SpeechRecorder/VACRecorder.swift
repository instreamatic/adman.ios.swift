/// Copyright 2018 Instreamatic
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import AudioToolbox
import Foundation

let VACRecorderNumberBuffers = 3
private let kNumberOfChanels = 1
private let kSampleRate: Float = 16000.0
private let kBitsPerChannel = 32
typealias VACRecorderState = (dataFormat: AudioStreamBasicDescription, queue: AudioQueueRef?, buffers: AudioQueueBufferRef?, audioFile: AudioFileID, bufferByteSize: UInt32, currentPacket: SInt64, running: Bool)

protocol VACRecorderDelegate: NSObjectProtocol {
    func processSample(
            withCapacity audioDataBytesCapacity: UInt32,
            audioData: UnsafeMutableRawPointer?
        )
}

func VACRecorderAudioInputCallback(
    _ inUserData: UnsafeMutableRawPointer?,
    _ inAQ: AudioQueueRef?,
    _ inBuffer: AudioQueueBufferRef?,
    _ inStartTime: AudioTimeStamp?,
    _ inNumberPackets: UInt32,
    _ inPacketDescs: AudioStreamPacketDescription?
) {

    let recorder = inUserData as? VACRecorder
    recorder?.delegate?.processSample(withCapacity: inBuffer?.mAudioDataBytesCapacity ?? 0, audioData: inBuffer?.mAudioData)
    recorder?.recordPackets += inNumberPackets

    if recorder?.running ?? false {
        if let queue = recorder?.state?.queue, let inBuffer = inBuffer {
            AudioQueueEnqueueBuffer(queue, inBuffer, 0, nil)
        }
    }
}

let VACRecorderCalculateBufferSizeMaxBufferSize = 0x50000

func VACRecorderCalculateBufferSize(
    _ audioStreamDescription: AudioStreamBasicDescription?,
    _ audioQueue: AudioQueueRef?,
    _ seconds: Float64,
    _ outBufferSize: UnsafeMutablePointer<UInt32>?
) {
    var outBufferSize = outBufferSize
    var maxPacketSize = Int(audioStreamDescription?.mBytesPerPacket ?? 0)

    if maxPacketSize == 0 {
        var maxVBRPacketSize = UInt32(MemoryLayout.size(ofValue: maxPacketSize))
        if let audioQueue = audioQueue {
            AudioQueueGetProperty(
                audioQueue,
                kAudioQueueProperty_MaximumOutputPacketSize,
                UnsafeMutableRawPointer(mutating: &maxPacketSize),
                UnsafeMutablePointer<UInt32>(mutating: &maxVBRPacketSize))
        }
    }

    let numBytesForTime = Float64(audioStreamDescription?.mSampleRate * maxPacketSize) * seconds
    outBufferSize = UnsafeMutablePointer<UInt32>(mutating: UInt32(numBytesForTime < VACRecorderCalculateBufferSizeMaxBufferSize
        ? numBytesForTime
        : VACRecorderCalculateBufferSizeMaxBufferSize))
}

class VACRecorder: NSObject {
    private var state: VACRecorderState?
    private var audioFile: AudioFileID?
    private var recordPackets: SInt64?

    weak var delegate: VACRecorderDelegate?

    private var _running = false
    var running: Bool {
        return state?._running ?? false
    }

    func setupAudioFormat16bit(_ format: AudioStreamBasicDescription?) {
        format?.mFormatID = kAudioFormatLinearPCM
        format?.mFormatFlags = kAudioFormatFlagIsSignedInteger
        format?.mBitsPerChannel = 16
        format?.mChannelsPerFrame = 1
        format?.mBytesPerFrame = 2
        format?.mBytesPerPacket = format?.mBytesPerFrame // (format->mBitsPerChannel / 8) * format->mChannelsPerFrame
        format?.mFramesPerPacket = 1
        format?.mSampleRate = Float64(kSampleRate)
    }

    func setupAudioFormat(_ format: AudioStreamBasicDescription?) {
        format?.mFormatID = kAudioFormatLinearPCM
        format?.mFormatFlags = kAudioFormatFlagsNativeFloatPacked
        format?.mBitsPerChannel = UInt32(kBitsPerChannel)
        format?.mChannelsPerFrame = UInt32(kNumberOfChanels)
        format?.mBytesPerFrame = 4
        format?.mBytesPerPacket = format?.mBytesPerFrame // (format->mBitsPerChannel / 8) * format->mChannelsPerFrame
        format?.mFramesPerPacket = 1
        format?.mSampleRate = Float64(kSampleRate)
    }

    func startRecording() {
        setupAudioFormat16bit(&state?.dataFormat)

        state?.currentPacket = 0
        var status = AudioQueueNewInput(
            &state?.dataFormat,
            VACRecorderAudioInputCallback,
            &self,
            CFRunLoopGetCurrent(),
            CFRunLoopMode.commonModes,
            0,
            &state?.queue)
        VACRecorderCalculateBufferSize(
            &state?.dataFormat)


        if Int(status) == 0 {

            for i in 0..<VACRecorderNumberBuffers {
                if let queue = state?.queue {
                    AudioQueueAllocateBuffer(queue, state?.bufferByteSize ?? 0, &state?.buffers[i])
                }
                if let queue = state?.queue, let buffers = state?.buffers[i] as? AudioQueueBufferRef {
                    AudioQueueEnqueueBuffer(queue, buffers, 0, nil)
                }
            }

            state?.running = true
            if let queue = state?.queue {
                status = AudioQueueStart(queue, nil)
            }
        }
    }

    func stopRecording() {
        state?.running = false

        if let queue = state?.queue {
            AudioQueueStop(queue, true)
        }
        if let audioFile = audioFile {
            AudioFileClose(audioFile)
        }

        for i in 0..<VACRecorderNumberBuffers {
            if let queue = state?.queue, let buffers = state?.buffers[i] as? AudioQueueBufferRef {
                AudioQueueFreeBuffer(queue, buffers)
            }
        }

        if let queue = state?.queue {
            AudioQueueDispose(queue, true)
        }
        if let audioFile1 = state?.audioFile {
            AudioFileClose(audioFile1)
        }
    }

    class func wavHeader() -> Data? {

        let headerSize = 44

        let totalAudioLen: UInt = 0 //use dummy for stream
        let totalDataLen: UInt = 0 //use dummy for stream
        let longSampleRate = UInt(kSampleRate)
        let channels = UInt(kNumberOfChanels)
        let byteRate = 2 * longSampleRate * channels
        let header = malloc(44) as? Byte
        header?[0] = "R" // RIFF/WAVE header
        header?[1] = "I"
        header?[2] = "F"
        header?[3] = "F"
        header?[4] = (totalDataLen & 0xff) as? Byte
        header?[5] = ((totalDataLen >> 8) & 0xff) as? Byte
        header?[6] = ((totalDataLen >> 16) & 0xff) as? Byte
        header?[7] = ((totalDataLen >> 24) & 0xff) as? Byte
        header?[8] = "W"
        header?[9] = "A"
        header?[10] = "V"
        header?[11] = "E"
        header?[12] = "f" // 'fmt ' chunk
        header?[13] = "m"
        header?[14] = "t"
        header?[15] = " "
        header?[16] = 16 // 4 bytes: size of 'fmt ' chunk
        header?[17] = nil
        header?[18] = nil
        header?[19] = nil
        header?[20] = 1 // format = 1
        header?[21] = nil
        header?[22] = channels as? Byte
        header?[23] = nil
        header?[24] = (longSampleRate & 0xff) as? Byte
        header?[25] = ((longSampleRate >> 8) & 0xff) as? Byte
        header?[26] = ((longSampleRate >> 16) & 0xff) as? Byte
        header?[27] = ((longSampleRate >> 24) & 0xff) as? Byte
        header?[28] = (byteRate & 0xff) as? Byte
        header?[29] = ((byteRate >> 8) & 0xff) as? Byte
        header?[30] = ((byteRate >> 16) & 0xff) as? Byte
        header?[31] = ((byteRate >> 24) & 0xff) as? Byte
        header?[32] = (kNumberOfChanels * 16 / 8) as? Byte // block align
        header?[33] = nil
        header?[34] = 16 // bits per sample
        header?[35] = nil
        header?[36] = "d"
        header?[37] = "a"
        header?[38] = "t"
        header?[39] = "a"
        header?[40] = (totalAudioLen & 0xff) as? Byte
        header?[41] = ((totalAudioLen >> 8) & 0xff) as? Byte
        header?[42] = ((totalAudioLen >> 16) & 0xff) as? Byte
        header?[43] = ((totalAudioLen >> 24) & 0xff) as? Byte

        return Data(bytes: &header, length: headerSize)
    }
}
