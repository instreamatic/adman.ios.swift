// swift-tools-version:5.0
import PackageDescription

let package = Package(
   name: "Adman-Swift",
   platforms: [
      .iOS(.v10)
   ],
   products: [
      .library(name: "Adman-Swift", targets: ["Adman-Swift"])
   ],
   dependencies: [
      .package(url: "https://github.com/Matovsky/SwiftTryCatch.git", .branch("master")),
      .package(url: "https://github.com/Matovsky/SocketRocket.git", .branch("master")),
   ],
   targets: [
      .target(
         name: "Adman-Swift",
         dependencies: [.byName(name: "SwiftTryCatch"), .byName(name: "SocketRocket")],
         path: "Adman-Swift"
      )
   ]
 )